## Trezi Raytracer

A Real time Path Tracer using Monte Carlo simulation for rendering based on NVIDIA Optix SDK and using GLFW and OpenGL for viewer.

## Quick Start
* You will need CMake to build the code. As of now this project only support Windows, so you need Visual Studio 2015 or 2017 in addition to CMake. 
* Install Optix 5.0 or higher and CUDA v9.2 or higher.

First, clone the code:
```
git clone https://surajsubudhi_smartvizx@bitbucket.org/surajsubudhi_smartvizx/trezi-raytracer.git --recursive
cd trezi-raytracer
```

Now into the project folder run the `win_builds.bat` file in command line.
If you have installed Optix SDK in different director you need to add the path of the Optix direction along with `win_builds.bat` batch file. 

```
\trezi-raytracer>.\win_builds.bat "< Optix SDK path inside double quote >"
```
It will generate .sln files along with separate visual studio projects file. Run the `TRay.sln` file.


## Features

* Unidirectional Path Tracing
* Lambertian and Microfacet BRDF
* Multiple Importance Sampling
* Mesh Loading
