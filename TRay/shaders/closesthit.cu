#include "app_config.h"

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include "rt_function.h"
#include "per_ray_data.h"
#include "material_parameter.h"
#include "light_definition.h"
#include "shader_common.h"
#include "device_include/random.h"
#include "device_include/commonStructs.h"

using namespace optix;

// Context global variables provided by the renderer system.
rtDeclareVariable(rtObject, sysTopObject, , );
rtDeclareVariable(float, sysSceneEpsilon, , );

// Semantic variables.
rtDeclareVariable(optix::Ray, theRay, rtCurrentRay, );
rtDeclareVariable(float, theIntersectionDistance, rtIntersectionDistance, );
rtDeclareVariable(float, theCurrentTime, rtCurrentTime, );

rtDeclareVariable(PerRayData, thePrd, rtPayload, );

// Attributes.
rtDeclareVariable(optix::float3, varGeoNormal, attribute GEO_NORMAL, );
//rtDeclareVariable(optix::float3, varTangent,   attribute TANGENT, );
rtDeclareVariable(optix::float3, varNormal, attribute NORMAL, );
rtDeclareVariable(optix::float3, varTexCoord, attribute TEXCOORD, );

// Material parameter definition.
rtBuffer<MaterialParameter> sysMaterialParameters; // Context global buffer with an array of structures of MaterialParameter.
rtDeclareVariable(int, parMaterialIndex, , ); // Per Material index into the sysMaterialParameters array.

rtBuffer<LightDefinition> sysLightDefinitions;
rtDeclareVariable(int, sysNumLights, , );     // PERF Used many times and faster to read than sysLightDefinitions.size().

// For Sun
rtBuffer<DirectionalLight> light_buffer;

rtBuffer< rtCallableProgramId<void(MaterialParameter const& parameters, State const& state, PerRayData& prd)> > sysSampleBSDF;

rtBuffer< rtCallableProgramId<float4(MaterialParameter const& parameters, State const& state, 
									 PerRayData& prd, float3 const& wiL)> > sysEvalBSDF;

rtBuffer< rtCallableProgramId<void(float3 const& point, const float2 sample, LightSample& lightSample)> > sysSampleLight;

__inline__ __device__ float3 tonemap(const float3 in)
{
	// hard coded exposure for sun/sky
	const float exposure = 1.0f / 30.0f;
	float3 x = exposure * in;
	// "filmic" map from a GDC talk by John Hable.  This includes 1/gamma.
	x = fmaxf(x - make_float3(0.004f), make_float3(0.0f));
	float3 ret = (x*(6.2f*x + make_float3(.5f))) / (x*(6.2f*x + make_float3(1.7f)) + make_float3(0.06f));
	return ret;
}

RT_FUNCTION float3 SunLight(State& state, MaterialParameter& material_parameter);

RT_PROGRAM void closesthit()
{
	State state; // All in world space coordinates!

	state.geoNormal = optix::normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, varGeoNormal));
	state.normal = optix::normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, varNormal));
	state.texcoord = varTexCoord;

	// Advance the path to the hit position in world coordinates.
	thePrd.pos = theRay.origin + theRay.direction * theIntersectionDistance; 
	// Return the current path segment distance, needed for absorption calculations in the integrator.
	thePrd.distance = theIntersectionDistance; 

	// Explicitly include edge-on cases as frontface condition!
	// Keeps the material stack from overflowing at silhouttes.
	// Prevents that silhouettes of thin-walled materials use the backface material.
	// Using the true geometry normal attribute as originally defined on the frontface!

	thePrd.flags |= (0.0f <= optix::dot(thePrd.wo, state.geoNormal)) ? (FLAG_FRONTFACE | FLAG_HIT) : FLAG_HIT;


	/*if ((thePrd.flags & FLAG_FRONTFACE) == 0) // Looking at the backface?
	{
		// Means geometric normal and shading normal are always defined on the side currently looked at.
		// This gives the backfaces of opaque BSDFs a defined result.
		state.geoNormal = -state.geoNormal;
		state.normal = -state.normal;
		// Do not recalculate the frontface condition!
	}*/

	// A material system with support for arbitrary mesh lights would evaluate its emission here.
	// But since only parallelogram area lights are supported, those get a dedicated closest hit program to simplify this demo.
	thePrd.radiance = make_float3(0.0f);

	// Copy the material parameters locally to be able to fetch texture data once.
	MaterialParameter parameters = sysMaterialParameters[parMaterialIndex]; 

	//PRINT3F("Plane Color", parameters.albedo);

	if (parameters.albedoID != RT_TEXTURE_ID_NULL)
	{
		const float3 texColor = make_float3(optix::rtTex2D<float4>(parameters.albedoID, state.texcoord.x, state.texcoord.y));

		// Modulate the incoming color with the texture.
		// linear color, resp. if the texture has been uint8 and readmode set to use sRGB, then sRGB.
		parameters.albedo *= texColor;               
		//parameters.albedo *= powf(texColor, 2.2f); // sRGB gamma correction done manually.
	}

	if (parameters.metallicTexID != RT_TEXTURE_ID_NULL)
	{
		const float3 texColor = make_float3(optix::rtTex2D<float4>(parameters.metallicTexID, state.texcoord.x, state.texcoord.y));

		// Modulate the incoming color with the texture.
		// linear color, resp. if the texture has been uint8 and readmode set to use sRGB, then sRGB.
		parameters.metallic = texColor.x;					
		//parameters.metallic *= powf(texColor.x, 2.2f);	// sRGB gamma correction done manually.
	}

	if (parameters.roughnessTexID != RT_TEXTURE_ID_NULL)
	{
		const float3 texColor = make_float3(optix::rtTex2D<float4>(parameters.roughnessTexID, state.texcoord.x, state.texcoord.y));

		// Modulate the incoming color with the texture.
		// linear color, resp. if the texture has been uint8 and readmode set to use sRGB, then sRGB.
		parameters.roughness = texColor.x;					
		//parameters.roughness *= powf(texColor.x, 2.2f);	// sRGB gamma correction done manually.
	}

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
	thePrd.albedo = parameters.albedo;
#if USE_DENOISER_NORMAL
	thePrd.normal = state.normal;
#endif
#endif
#endif

	// Start fresh with the next BSDF sample.  
	// (Either of these values remaining zero is an end-of-path condition.)
	thePrd.f_over_pdf = make_float3(0.0f);
	thePrd.pdf = 0.0f;

	// Only the last diffuse hit is tracked for multiple importance sampling of implicit light hits.
	// FLAG_THINWALLED can be set directly from the material parameters.
	thePrd.flags = (thePrd.flags & ~FLAG_DIFFUSE) | parameters.flags; 

    //thePrd.wo = -theRay.direction;
	sysSampleBSDF[parameters.indexBSDF](parameters, state, thePrd);
	const float4 bsdf_pdf = sysEvalBSDF[parameters.indexBSDF](parameters, state, thePrd, thePrd.wi);

	thePrd.f_over_pdf = make_float3(bsdf_pdf) * fabsf(optix::dot(thePrd.wi, state.normal)) / thePrd.pdf;


#if USE_NEXT_EVENT_ESTIMATION
	// Direct lighting if the sampled BSDF was diffuse and any light is in the scene.
	if ((thePrd.flags & FLAG_DIFFUSE) && 0 < sysNumLights)
	{
		const float2 sample = rng2(thePrd.seed); // Use lower dimension samples for the position. (Irrelevant for the LCG).

		LightSample lightSample; // Sample one of many lights. 

		// The caller picks the light to sample. Make sure the index stays in the bounds of the sysLightDefinitions array.
		//lightSample.index = optix::clamp(static_cast<int>(floorf(rng(thePrd.seed) * sysNumLights)), 0, sysNumLights - 1);
		int randIndexVal = static_cast<int>(floorf(rng(thePrd.seed) * (sysNumLights + 1)));
		lightSample.index = optix::clamp(randIndexVal, 0, sysNumLights);
		if (lightSample.index == 0)
		{
			const LightType lightType = sysLightDefinitions[lightSample.index].type;
			sysSampleLight[lightType](thePrd.pos, sample, lightSample);
		}
		else
		{
			PRINT_MSG("Test 02 - SUN");
			const DirectionalLight& light = light_buffer[0];
			float3 fhp = thePrd.pos;
			const float3 light_center = fhp + light.direction;
			const float r1 = rnd(thePrd.seed);
			const float r2 = rnd(thePrd.seed);
			const float2 disk_sample = square_to_disk(make_float2(r1, r2));
			const float3 jittered_pos = light_center + light.radius*disk_sample.x*light.v0 + light.radius*disk_sample.y*light.v1;

			lightSample.position = jittered_pos;
			lightSample.direction = normalize(jittered_pos - fhp);
			lightSample.distance = RT_DEFAULT_MAX;
			lightSample.emission = light.color;
			lightSample.pdf = light.radius*light.radius*M_PIf;
		}
		

		if (0.0f < lightSample.pdf) // Useful light sample?
		{
			// Evaluate the BSDF in the light sample direction. Normally cheaper than shooting rays.
			// Returns BSDF f in .xyz and the BSDF pdf in .w
			const float4 bsdf_pdf = sysEvalBSDF[parameters.indexBSDF](parameters, state, thePrd, lightSample.direction);

			if (0.0f < bsdf_pdf.w && isNotNull(make_float3(bsdf_pdf)))
			{
				// Do the visibility check of the light sample.
				PerRayData_shadow prdShadow;

				prdShadow.seed = thePrd.seed; // For potential stochastic cutout opacity sampling.
				prdShadow.visible = true;        // Initialize for miss.

				// Note that the sysSceneEpsilon is applied on both sides of the shadow ray [t_min, t_max] interval 
				// to prevent self intersections with the actual light geometry in the scene!
				optix::Ray ray = optix::make_Ray(thePrd.pos, lightSample.direction, 1, sysSceneEpsilon, 
																lightSample.distance - sysSceneEpsilon); // Shadow ray.
				rtTrace(sysTopObject, ray, theCurrentTime, prdShadow);

				thePrd.seed = prdShadow.seed; // Continue the RNG state!

				if (prdShadow.visible)
				{
					if (thePrd.flags & FLAG_VOLUME) // Supporting nested materials includes having lights inside a volume.
					{
						// Calculate the transmittance along the light sample's distance in case it's inside a volume.
						// The light must be in the same volume or it would have been shadowed!
						lightSample.emission *= expf(-lightSample.distance * thePrd.extinction);
					}

					const float misWeight = powerHeuristic(lightSample.pdf, bsdf_pdf.w);

					float NDotL = optix::dot(lightSample.direction, state.normal);

					if (NDotL < 0.0f)
					{
						thePrd.radiance += make_float3(0.0f, 0.0f, 0.0f);
						thePrd.flags = thePrd.flags | FLAG_TERMINATE;
					}
					else
					{
						thePrd.radiance += make_float3(bsdf_pdf) * lightSample.emission * (misWeight * NDotL / lightSample.pdf);
					}
				}
			}
		}
	}

	
	/*if ((thePrd.flags & FLAG_DIFFUSE))
	{
		float3 sunCon = SunLight(state, parameters);
		thePrd.radiance += sunCon;
	}*/
#endif // USE_NEXT_EVENT_ESTIMATION


}

RT_FUNCTION float3 SunLight(State& state, MaterialParameter& material_parameter)
{
	float3 sunContribution = make_float3(0.0f);

	const DirectionalLight& light = light_buffer[0];
	float3 fhp = thePrd.pos;
	const float3 light_center = fhp + light.direction;
	const float r1 = rnd(thePrd.seed);
	const float r2 = rnd(thePrd.seed);
	const float2 disk_sample = square_to_disk(make_float2(r1, r2));
	const float3 jittered_pos = light_center + light.radius*disk_sample.x*light.v0 + light.radius*disk_sample.y*light.v1;
	const float3 L = normalize(jittered_pos - fhp);

	const float NdotL = dot(state.normal, L);
	if (NdotL > 0.0f)
	{
		PerRayData_shadow shadow_prd;
		shadow_prd.visible = true;
		optix::Ray shadow_ray(fhp, L, /*shadow ray type*/ 1, sysSceneEpsilon);
		rtTrace(sysTopObject, shadow_ray, shadow_prd);

		if (shadow_prd.visible)
		{
			float lightRadSqr = light.radius*light.radius;
			const float solid_angle = lightRadSqr*M_PIf;
			float3 SunCon = NdotL * light.color * solid_angle;
			sunContribution += SunCon;
		}
	}

	return sunContribution;
}