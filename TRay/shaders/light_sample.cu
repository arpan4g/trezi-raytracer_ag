#include "app_config.h"

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include "rt_function.h"
#include "per_ray_data.h"
#include "light_definition.h"
#include "shader_common.h"
#include "device_include/helpers.h"

#include "rt_assert.h"

using namespace optix;

rtBuffer<LightDefinition> sysLightDefinitions;

rtDeclareVariable(int, sysNumLights, , ); // PERF Used many times and faster to read than sysLightDefinitions.size().
rtDeclareVariable(float, sysEnvironmentRotation, , );


// -----------------------------------------------------------------------------
// Sky Model
// -----------------------------------------------------------------------------

rtDeclareVariable(float, overcast, , );
rtDeclareVariable(optix::float3, sun_direction, , );
rtDeclareVariable(optix::float3, sun_color, , );
rtDeclareVariable(optix::float3, sky_up, , );

rtDeclareVariable(optix::float3, inv_divisor_Yxy, , );
rtDeclareVariable(optix::float3, c0, , );
rtDeclareVariable(optix::float3, c1, , );
rtDeclareVariable(optix::float3, c2, , );
rtDeclareVariable(optix::float3, c3, , );
rtDeclareVariable(optix::float3, c4, , );

static __host__ __device__ __inline__ optix::float3 querySkyModel(bool CEL, const optix::float3& direction)
{
	using namespace optix;

	float3 sunlit_sky_color = make_float3(0.0f);

	float3 skyUp = sky_up;
	//float3 skyUp = make_float3(0.0f, 0.0f, 1.0f);

	// Preetham skylight model
	if (overcast < 1.0f)
	{
		float3 ray_direction = direction;
		if (CEL && dot(ray_direction, sun_direction) > 94.0f / sqrtf(94.0f*94.0f + 0.45f*0.45f))
		{
			sunlit_sky_color = sun_color;
			//sunlit_sky_color = make_float3(1.0f, 0.0f, 0.0f);
		}
		else
		{
			float inv_dir_dot_up = 1.f / dot(ray_direction, skyUp);
			if (inv_dir_dot_up < 0.f) {
				ray_direction = reflect(ray_direction, skyUp);
				inv_dir_dot_up = -inv_dir_dot_up;
			}

			float gamma = dot(sun_direction, ray_direction);
			float acos_gamma = acosf(gamma);
			float3 A = c1 * inv_dir_dot_up;
			float3 B = c3 * acos_gamma;
			float3 color_Yxy = (make_float3(1.0f) + c0 * make_float3(expf(A.x), expf(A.y), expf(A.z))) *
				(make_float3(1.0f) + c2 * make_float3(expf(B.x), expf(B.y), expf(B.z)) + c4 * gamma*gamma);
			color_Yxy *= inv_divisor_Yxy;

			color_Yxy.y = 0.33f + 1.2f * (color_Yxy.y - 0.33f); // Pump up chromaticity a bit
			color_Yxy.z = 0.33f + 1.2f * (color_Yxy.z - 0.33f); //
			float3 color_XYZ = Yxy2XYZ(color_Yxy);
			sunlit_sky_color = XYZ2rgb(color_XYZ);
			sunlit_sky_color /= 1000.0f; // We are choosing to return kilo-candellas / meter^2
		}
	}

	// CIE standard overcast sky model
	float Y = 15.0f;
	float3 overcast_sky_color = make_float3((1.0f + 2.0f * fabsf(direction.z)) / 3.0f * Y);

	// return linear combo of the two
	return lerp(sunlit_sky_color, overcast_sky_color, overcast);
}

// -----------------------------------------------------------------------------



RT_FUNCTION void unitSquareToSphere(const float u, const float v, float3& p, float& pdf)
{
	p.z = 1.0f - 2.0f * u;
	float r = 1.0f - p.z * p.z;
	r = (0.0f < r) ? sqrtf(r) : 0.0f;

	const float phi = v * 2.0f * M_PIf;
	p.x = r * cosf(phi);
	p.y = r * sinf(phi);

	pdf = 0.25f * M_1_PIf;  // == 1.0f / (4.0f * M_PIf)
}

// Note that all light sampling routines return lightSample.direction and lightSample.distance in world space!

RT_CALLABLE_PROGRAM void sample_light_constant(float3 const& point, const float2 sample, LightSample& lightSample)
{
	unitSquareToSphere(sample.x, sample.y, lightSample.direction, lightSample.pdf);

	// Environment lights do not set the light sample position!
	lightSample.distance = RT_DEFAULT_MAX; // Environment light.

	// Explicit light sample. White scaled by inverse probabilty to hit this light.
	lightSample.emission = make_float3(sysNumLights);
}

RT_CALLABLE_PROGRAM void sample_light_environment(float3 const& point, const float2 sample, LightSample& lightSample)
{
	const LightDefinition light = sysLightDefinitions[0]; // The environment light is always placed into the first entry.

	// Importance-sample the spherical environment light direction.
	const unsigned int sizeU = static_cast<unsigned int>(light.idEnvironmentCDF_U.size().x);
	const unsigned int sizeV = static_cast<unsigned int>(light.idEnvironmentCDF_V.size());

	unsigned int ilo = 0;          // Use this for full spherical lighting. (This matches the result of indirect environment lighting.)
	unsigned int ihi = sizeV - 1; // Index on the last entry containing 1.0f. Can never be reached with the sample in the range [0.0f, 1.0f).

	// Binary search the row index to look up.
	while (ilo != ihi - 1) // When a pair of limits have been found, the lower index indicates the cell to use.
	{
		const unsigned int i = (ilo + ihi) >> 1;
		const float cdf = light.idEnvironmentCDF_V[i];
		if (sample.y < cdf) // If the cdf is greater than the sample, use that as new higher limit.
		{
			ihi = i;
		}
		else // If the sample is greater than or equal to the CDF value, use that as new lower limit.
		{
			ilo = i;
		}
	}

	uint2 index; // 2D index used in the next binary search.
	index.y = ilo; // This is the row we found.

	// Binary search the column index to look up.
	ilo = 0;
	ihi = sizeU - 1; // Index on the last entry containing 1.0f. Can never be reached with the sample in the range [0.0f, 1.0f).
	while (ilo != ihi - 1) // When a pair of limits have been found, the lower index indicates the cell to use.
	{
		index.x = (ilo + ihi) >> 1;
		const float cdf = light.idEnvironmentCDF_U[index];
		if (sample.x < cdf) // If the CDF value is greater than the sample, use that as new higher limit.
		{
			ihi = index.x;
		}
		else // If the sample is greater than or equal to the CDF value, use that as new lower limit.
		{
			ilo = index.x;
		}
	}

	index.x = ilo; // The column result.

	// Continuous sampling of the CDF.
	const float cdfLowerU = light.idEnvironmentCDF_U[index];
	const float cdfUpperU = light.idEnvironmentCDF_U[make_uint2(index.x + 1, index.y)];
	const float du = (sample.x - cdfLowerU) / (cdfUpperU - cdfLowerU);

	const float cdfLowerV = light.idEnvironmentCDF_V[index.y];
	const float cdfUpperV = light.idEnvironmentCDF_V[index.y + 1];
	const float dv = (sample.y - cdfLowerV) / (cdfUpperV - cdfLowerV);

	// Texture lookup coordinates.
	const float u = (float(index.x) + du) / float(sizeU - 1);
	const float v = (float(index.y) + dv) / float(sizeV - 1);

	// Light sample direction vector polar coordinates. This is where the environment rotation happens!
	// DAR FIXME Use a light.matrix to rotate the resulting vector instead.
	const float phi = (u - sysEnvironmentRotation) * 2.0f * M_PIf;
	const float theta = v * M_PIf; // theta == 0.0f is south pole, theta == M_PIf is north pole.

	const float sinTheta = sinf(theta);
	// The miss program places the 1->0 seam at the positive z-axis and looks from the inside.
	lightSample.direction = make_float3(-sinf(phi) * sinTheta,  // Starting on positive z-axis going around clockwise (to negative x-axis).
		-cosf(theta),           // From south pole to north pole.
		cosf(phi) * sinTheta); // Starting on positive z-axis.

// Note that environment lights do not set the light sample position!
	lightSample.distance = RT_DEFAULT_MAX; // Environment light.

	const float3 emission = make_float3(optix::rtTex2D<float4>(light.idEnvironmentTexture, u, v));
	// Explicit light sample. The returned emission must be scaled by the inverse probability to select this light.
	lightSample.emission = emission * sysNumLights;
	// For simplicity we pretend that we perfectly importance-sampled the actual texture-filtered environment map
	// and not the Gaussian-smoothed one used to actually generate the CDFs and uniform sampling in the texel.
	lightSample.pdf = intensity(emission) / light.environmentIntegral;
}

RT_CALLABLE_PROGRAM void sample_light_sky_model(float3 const& point, const float2 sample, LightSample& lightSample)
{
	unitSquareToSphere(sample.x, sample.y, lightSample.direction, lightSample.pdf);

	// Environment lights do not set the light sample position!
	lightSample.distance = RT_DEFAULT_MAX; // Environment light.
	float3 sunRad = querySkyModel(true, lightSample.direction);
	// Explicit light sample. White scaled by inverse probabilty to hit this light.
	//PRINT_MSG("In Sky model");
	lightSample.emission = make_float3(1.0f, 0.0f, 0.0f);// sunRad * make_float3(sysNumLights);
}

RT_CALLABLE_PROGRAM void sample_light_parallelogram(float3 const& point, const float2 sample, LightSample& lightSample)
{
	lightSample.pdf = 0.0f; // Default return, invalid light sample (backface, edge on, or too near to the surface)

	const LightDefinition light = sysLightDefinitions[lightSample.index]; // The light index is picked by the caller!

	lightSample.position = light.position + light.vecU * sample.x + light.vecV * sample.y; // The light sample position in world coordinates.
	lightSample.direction = lightSample.position - point; // Sample direction from surface point to light sample position.
	lightSample.distance = optix::length(lightSample.direction);
	if (DENOMINATOR_EPSILON < lightSample.distance)
	{
		lightSample.direction /= lightSample.distance; // Normalized direction to light.

		const float cosTheta = optix::dot(-lightSample.direction, light.normal);
		if (DENOMINATOR_EPSILON < cosTheta) // Only emit light on the front side.
		{
			// Explicit light sample, must scale the emission by inverse probabilty to hit this light.
			lightSample.emission = light.emission * float(sysNumLights);
			lightSample.pdf = (lightSample.distance * lightSample.distance) / (light.area * cosTheta); // Solid angle pdf. Assumes light.area != 0.0f.
		}
	}
}
