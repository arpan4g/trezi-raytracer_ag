#pragma once

#ifndef RT_FUNCTION_H
#define RT_FUNCTION_H

#ifndef RT_FUNCTION
#define RT_FUNCTION __forceinline__ __device__
#endif

// HELPER MACROS
#define PRINT_MSG(msg)	  rtPrintf(msg); rtPrintf("\n");
#define PRINT3F(msg, val) rtPrintf(msg); rtPrintf(" : (%f, %f, %f) \n", val.x, val.y, val.z);
#define PRINT1F(msg, val) rtPrintf(msg); rtPrintf(" : %f \n", val);
#define PRINT1I(msg, val) rtPrintf(msg); rtPrintf(" : %d \n", val);


#define CHECK_RADIANCE(rad) if(rad.x < 0.0f || rad.y < 0.0f || rad.z < 0.0f) \
	{ \
		rad = make_float3(1000000.0f, 0.0f, 0.0f); \
	} \

#endif // RT_FUNCTION_H
