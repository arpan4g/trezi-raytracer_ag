#include "app_config.h"

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include "rt_function.h"
#include "material_parameter.h"
#include "per_ray_data.h"

RT_CALLABLE_PROGRAM void sample_bsdf_specular_reflection(MaterialParameter const& parameters, State const& state, PerRayData& prd)
{
	prd.wi = optix::reflect(-prd.wo, state.normal);

	if (optix::dot(prd.wi, state.geoNormal) <= 0.0f) // Do not sample opaque materials below the geometric surface.
	{
		prd.flags |= FLAG_TERMINATE;
		return;
	}

	//prd.f_over_pdf = parameters.albedo;
	prd.pdf = 1.0f; // Not 0.0f to make sure the path is not terminated. Otherwise unused for specular events.
}

// This is actually never reached, because the FLAG_DIFFUSE flag is not set when a specular BSDF is has been sampled.
RT_CALLABLE_PROGRAM float4 eval_bsdf_specular_reflection(MaterialParameter const& parameters, State const& state, PerRayData& prd, float3 const& wiL)
{
	return make_float4(parameters.albedo / fabsf(optix::dot(wiL, state.normal)), 1.0f);
	// return make_float4(0.0f);
}

