/*
 Copyright Disney Enterprises, Inc.  All rights reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License
 and the following modification to it: Section 6 Trademarks.
 deleted and replaced with:

 6. Trademarks. This License does not grant permission to use the
 trade names, trademarks, service marks, or product names of the
 Licensor and its affiliates, except as required for reproducing
 the content of the NOTICE file.

 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
*/

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu_matrix_namespace.h>

#include "rt_function.h"
#include "material_parameter.h"
#include "random_number_generators.h"
#include "per_ray_data.h"


using namespace optix;

rtDeclareVariable(Ray, ray, rtCurrentRay, );
rtDeclareVariable(float, t_hit, rtIntersectionDistance, );

// Material Params
rtDeclareVariable(float, sysMatRoughness, , );
rtDeclareVariable(float, sysMatMetallic, , );
rtDeclareVariable(float, sysMatSpecular, , );

RT_FUNCTION float sqr(float x)
{
	return x * x;
}

RT_FUNCTION float SchlickFresnel(float u)
{
	float m = clamp(1.0f - u, 0.0f, 1.0f);
	float m2 = m * m;
	return m2 * m2*m; // pow(m,5)
}

RT_FUNCTION float GTR1(float NDotH, float a)
{
	if (a >= 1.0f) return (1.0f / M_PIf);
	float a2 = a * a;
	float t = 1.0f + (a2 - 1.0f)*NDotH*NDotH;
	return (a2 - 1.0f) / (M_PIf*logf(a2)*t);
}

RT_FUNCTION float GTR2(float NDotH, float a)
{
	float a2 = a * a;
	float t = 1.0f + (a2 - 1.0f)*NDotH*NDotH;
	return a2 / (M_PIf * t*t);
}

RT_FUNCTION float smithG_GGX(float NDotv, float alphaG)
{
	float a = alphaG * alphaG;
	float b = NDotv * NDotv;
	return 1.0f / (NDotv + sqrtf(a + b - a * b));
}


/*
	http://simon-kallweit.me/rendercompo2015/
*/
RT_FUNCTION void Pdf(MaterialParameter const& mat, State const& state, PerRayData &prd)
{
    float3 n = state.normal;
    float3 V = prd.wo;
	//float3 V = -ray.direction;
	float3 L = prd.wi;

	float roughness			= mat.roughness;
	float metallicVal		= mat.metallic;


	float clearCoatGloss	= 0.0f; //mat.clearcoatGloss
	float clearCoat			= 0.0f; //mat.clearcoat

	float specularAlpha = max(0.001f, roughness);
	float clearcoatAlpha = lerp(0.1f, 0.001f, clearCoatGloss);

	float diffuseRatio = 0.5f * (1.f - metallicVal);
	float specularRatio = 1.f - diffuseRatio;

	float3 half = normalize(L + V);

	float cosTheta = abs(dot(half, n));
	float pdfGTR2 = GTR2(cosTheta, specularAlpha) * cosTheta;
	float pdfGTR1 = GTR1(cosTheta, clearcoatAlpha) * cosTheta;

	// calculate diffuse and specular pdfs and mix ratio
	float ratio = 1.0f / (1.0f + clearCoat);
	float pdfSpec = lerp(pdfGTR1, pdfGTR2, ratio) / (4.0f * abs(dot(L, half)));
	float pdfDiff = abs(dot(L, n))* (1.0f / M_PIf);

	// weight pdfs according to ratios
	prd.pdf = diffuseRatio * pdfDiff + specularRatio * pdfSpec;

}


RT_CALLABLE_PROGRAM float4 eval_disney(MaterialParameter const& mat, State const& state, PerRayData& prd, float3 const& wiL)
{
    float3 N = state.normal;
    float3 V = prd.wo;
	//float3 V = -ray.direction;
	float3 L = wiL;

	float specular			= mat.specular;
	float roughness			= mat.roughness;
	float metallicVal		= mat.metallic;

	// Default values are 0.0f as we are not using these parameter in Trezi
	float sheenTint			= 0.0f; //mat.sheenTint;
	float specularTint		= 0.0f; //mat.specularTint;
	float clearCoatGloss	= 0.0f; //mat.clearcoatGloss;
	float clearCoat			= 0.0f; //mat.clearcoat;
	float subsurface		= 0.0f; //mat.subsurface;
	float sheen				= 0.0f; //mat.seen;


	float NDotL = dot(N, L);
	float NDotV = dot(N, V);

	if (NDotL <= 0.0f || NDotV <= 0.0f) 
	{
		prd.flags |= FLAG_TERMINATE;
		return make_float4(0.0f);
	}

	float3 H = normalize(L + V);
	float NDotH = dot(N, H);
	float LDotH = dot(L, H);

	float3 Cdlin = mat.albedo;
	float Cdlum = 0.3f*Cdlin.x + 0.6f*Cdlin.y + 0.1f*Cdlin.z; // luminance approx.

	float3 Ctint = Cdlum > 0.0f ? Cdlin / Cdlum : make_float3(1.0f); // normalize lum. to isolate hue+sat
	float3 Cspec0 = lerp(specular * 0.08f * lerp(make_float3(1.0f), Ctint, specularTint), Cdlin, metallicVal);
	float3 Csheen = lerp(make_float3(1.0f), Ctint, sheenTint);

	// Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
	// and mix in diffuse retro-reflection based on roughness
	float FL = SchlickFresnel(NDotL), FV = SchlickFresnel(NDotV);
	float Fd90 = 0.5f + 2.0f * LDotH*LDotH * roughness;
	float Fd = lerp(1.0f, Fd90, FL) * lerp(1.0f, Fd90, FV);

	// Based on Hanrahan-Krueger brdf approximation of isotrokPic bssrdf
	// 1.25 scale is used to (roughly) preserve albedo
	// Fss90 used to "flatten" retroreflection based on roughness
	float Fss90 = LDotH * LDotH * roughness;
	float Fss = lerp(1.0f, Fss90, FL) * lerp(1.0f, Fss90, FV);
	float ss = 1.25f * (Fss * (1.0f / (NDotL + NDotV) - 0.5f) + 0.5f);

	// specular
	//float aspect = sqrt(1-mat.anisotrokPic*.9);
	//float ax = Max(.001f, sqr(roughness)/aspect);
	//float ay = Max(.001f, sqr(roughness)*aspect);
	//float Ds = GTR2_aniso(NDotH, Dot(H, X), Dot(H, Y), ax, ay);

	float a = max(0.001f, roughness);
	float Ds = GTR2(NDotH, a);
	float FH = SchlickFresnel(LDotH);
	float3 Fs = lerp(Cspec0, make_float3(1.0f), FH);
	float roughg = sqr(roughness*0.5f + 0.5f);
	float Gs = smithG_GGX(NDotL, roughg) * smithG_GGX(NDotV, roughg);

	// sheen
	float3 Fsheen = FH * sheen * Csheen;

	// clearcoat (ior = 1.5 -> F0 = 0.04)
	float Dr = GTR1(NDotH, lerp(0.1f, 0.001f, clearCoatGloss));
	float Fr = lerp(0.04f, 1.0f, FH);
	float Gr = smithG_GGX(NDotL, 0.25f) * smithG_GGX(NDotV, 0.25f);

	float3 diffuseVal = ((1.0f / M_PIf) * lerp(Fd, ss, subsurface)*Cdlin + Fsheen) * (1.0f - metallicVal);
	float3 specularVal = Gs * Fs * Ds;
	float clearCoatVal = 0.25f * clearCoat * Gr * Fr * Dr;
	
	float3 out = (diffuseVal + specularVal + clearCoatVal) * clamp(dot(N, L), 0.0f, 1.0f);
	return make_float4(out, prd.pdf);
}


/*
	https://learnopengl.com/PBR/IBL/Specular-IBL
*/

RT_CALLABLE_PROGRAM void sample_disney(MaterialParameter const& mat, State const& state, PerRayData& prd)
{
    float3 N = state.normal;
    float3 V = prd.wo;
	//float3 V = -ray.direction;
	//prd.pos = state.fhp;

	float roughness		= mat.roughness;
	float metallicVal	= mat.metallic;

	float3 dir;

	float probability = rng(prd.seed);
	float diffuseRatio = 0.5f * (1.0f - metallicVal);

	float r1 = rng(prd.seed);
	float r2 = rng(prd.seed);

	optix::Onb onb(N); // basis

	if (probability < diffuseRatio) // sample diffuse
	{
		cosine_sample_hemisphere(r1, r2, dir);
		onb.inverse_transform(dir);
		prd.flags |= FLAG_DIFFUSE;
	}
	else
	{
		float a = max(0.001f, roughness);

		float phi = r1 * 2.0f * M_PIf;

		float cosTheta = sqrtf((1.0f - r2) / (1.0f + (a*a - 1.0f) *r2));
		float sinTheta = sqrtf(1.0f - (cosTheta * cosTheta));
		float sinPhi = sinf(phi);
		float cosPhi = cosf(phi);

		float3 half = make_float3(sinTheta*cosPhi, sinTheta*sinPhi, cosTheta);
		onb.inverse_transform(half);

		dir = 2.0f*dot(V, half)*half - V; //reflection vector

	}
	prd.wi = dir;
	Pdf(mat, state, prd);
}

