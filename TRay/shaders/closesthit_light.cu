#include "app_config.h"

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include "rt_function.h"

#include "per_ray_data.h"
#include "light_definition.h"
#include "shader_common.h"

// Context global variables provided by the renderer system.
rtDeclareVariable(rtObject, sysTopObject, , );
//rtDeclareVariable(float,    sysSceneEpsilon, , );

// Semantic variables.
rtDeclareVariable(optix::Ray, theRay, rtCurrentRay, );
rtDeclareVariable(float, theIntersectionDistance, rtIntersectionDistance, );

rtDeclareVariable(PerRayData, thePrd, rtPayload, );

// Attributes.
rtDeclareVariable(optix::float3, varGeoNormal, attribute GEO_NORMAL, );
//rtDeclareVariable(optix::float3, varTangent,   attribute TANGENT, );
//rtDeclareVariable(optix::float3, varNormal,    attribute NORMAL, ); 
//rtDeclareVariable(optix::float3, varTexCoord,  attribute TEXCOORD, ); 

rtBuffer<LightDefinition> sysLightDefinitions;
rtDeclareVariable(int, parLightIndex, , );  // Index into the sysLightDefinitions array.

// Very simple closest hit program just for rectangle area lights.
RT_PROGRAM void closesthit_light()
{
	thePrd.pos = theRay.origin + theRay.direction * theIntersectionDistance; // Advance the path to the hit position in world coordinates.
	thePrd.distance = theIntersectionDistance; // Return the current path segment distance, needed for absorption calculations in the integrator.

	const float3 geoNormal = optix::normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, varGeoNormal)); // PERF Not really needed when it's know that light geometry is not under Transforms.

	const float cosTheta = optix::dot(thePrd.wo, geoNormal);
	thePrd.flags |= (0.0f <= cosTheta) ? (FLAG_FRONTFACE | FLAG_HIT) : FLAG_HIT;

	const LightDefinition light = sysLightDefinitions[parLightIndex];

	thePrd.radiance = make_float3(0.0f); // Backside is black.

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
	thePrd.albedo = make_float3(0.0f); // Backside is black.
#if USE_DENOISER_NORMAL
	thePrd.normal = -light.normal;
#endif
#endif
#endif

	if (thePrd.flags & FLAG_FRONTFACE) // Looking at the front face?
	{
		thePrd.radiance = light.emission;

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
		thePrd.albedo = light.emission;
#if USE_DENOISER_NORMAL
		thePrd.normal = light.normal;
#endif
#endif
#endif

#if USE_NEXT_EVENT_ESTIMATION
		const float pdfLight = (thePrd.distance * thePrd.distance) / (light.area * cosTheta); // Solid angle pdf. Assumes light.area != 0.0f.
		// If it's an implicit light hit from a diffuse scattering event and the light emission was not returning a zero pdf.
		if ((thePrd.flags & FLAG_DIFFUSE) && DENOMINATOR_EPSILON < pdfLight)
		{
			// Scale the emission with the power heuristic between the previous BSDF sample pdf and this implicit light sample pdf.
			thePrd.radiance *= powerHeuristic(thePrd.pdf, pdfLight);
		}
#endif // USE_NEXT_EVENT_ESTIMATION
	}

	// Lights have no other material properties than emission in this demo. Terminate the path.
	thePrd.flags |= (FLAG_LIGHT | FLAG_TERMINATE);
}

