#include "app_config.h"

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include "rt_function.h"
#include "per_ray_data.h"
#include "light_definition.h"
#include "shader_common.h"
#include "device_include/helpers.h"

using namespace  optix;

rtDeclareVariable(optix::Ray, theRay, rtCurrentRay, );
rtDeclareVariable(PerRayData, thePrd, rtPayload, );
rtDeclareVariable(float, sysEnvironmentRotation, , );

rtBuffer<LightDefinition> sysLightDefinitions;

#define USE_SKY_MODEL 1

__inline__ __device__ float3 tonemap(const float3 in)
{
	// hard coded exposure for sun/sky
	const float exposure = 1.0f / 30.0f;
	float3 x = exposure * in;
	// "filmic" map from a GDC talk by John Hable.  This includes 1/gamma.
	x = fmaxf(x - make_float3(0.004f), make_float3(0.0f));
	float3 ret = (x*(6.2f*x + make_float3(.5f))) / (x*(6.2f*x + make_float3(1.7f)) + make_float3(0.06f));
	return ret;
}

// -----------------------------------------------------------------------------
// Sky Model
// -----------------------------------------------------------------------------

rtDeclareVariable(float, overcast, , );
rtDeclareVariable(optix::float3, sun_direction, , );
rtDeclareVariable(optix::float3, sun_color, , );
rtDeclareVariable(optix::float3, sky_up, , );

rtDeclareVariable(optix::float3, inv_divisor_Yxy, , );
rtDeclareVariable(optix::float3, c0, , );
rtDeclareVariable(optix::float3, c1, , );
rtDeclareVariable(optix::float3, c2, , );
rtDeclareVariable(optix::float3, c3, , );
rtDeclareVariable(optix::float3, c4, , );

static __host__ __device__ __inline__ optix::float3 querySkyModel(bool CEL, const optix::float3& direction)
{
	using namespace optix;

	float3 overcast_sky_color = make_float3(0.0f);
	float3 sunlit_sky_color = make_float3(0.0f);

	float3 skyUp = sky_up;
	//float3 skyUp = make_float3(0.0f, 0.0f, 1.0f);

	// Preetham skylight model
	if (overcast < 1.0f)
	{
		float3 ray_direction = direction;
		if (CEL && dot(ray_direction, sun_direction) > 94.0f / sqrtf(94.0f*94.0f + 0.45f*0.45f))
		{
			sunlit_sky_color = sun_color;
			//sunlit_sky_color = make_float3(1.0f, 0.0f, 0.0f);
		}
		else
		{
			float inv_dir_dot_up = 1.f / dot(ray_direction, skyUp);
			if (inv_dir_dot_up < 0.f) {
				ray_direction = reflect(ray_direction, skyUp);
				inv_dir_dot_up = -inv_dir_dot_up;
			}

			float gamma = dot(sun_direction, ray_direction);
			float acos_gamma = acosf(gamma);
			float3 A = c1 * inv_dir_dot_up;
			float3 B = c3 * acos_gamma;
			float3 color_Yxy = (make_float3(1.0f) + c0 * make_float3(expf(A.x), expf(A.y), expf(A.z))) *
				(make_float3(1.0f) + c2 * make_float3(expf(B.x), expf(B.y), expf(B.z)) + c4 * gamma*gamma);
			color_Yxy *= inv_divisor_Yxy;

			color_Yxy.y = 0.33f + 1.2f * (color_Yxy.y - 0.33f); // Pump up chromaticity a bit
			color_Yxy.z = 0.33f + 1.2f * (color_Yxy.z - 0.33f); //
			float3 color_XYZ = Yxy2XYZ(color_Yxy);
			sunlit_sky_color = XYZ2rgb(color_XYZ);
			sunlit_sky_color /= 1000.0f; // We are choosing to return kilo-candellas / meter^2
		}
	}

	// CIE standard overcast sky model
	float Y = 15.0f;
	overcast_sky_color = make_float3((1.0f + 2.0f * fabsf(direction.z)) / 3.0f * Y);

	// return linear combo of the two
	return lerp(sunlit_sky_color, overcast_sky_color, overcast);
}



// Not actually a light. Never appears inside the sysLightDefinitions.
RT_PROGRAM void miss_environment_null()
{
	thePrd.radiance = make_float3(0.0f);

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
	thePrd.albedo = make_float3(0.0f);
#endif
#endif

	thePrd.flags |= FLAG_TERMINATE; // Not a light.
}

RT_PROGRAM void miss_environment_constant()
{
#if USE_NEXT_EVENT_ESTIMATION
	// If the last surface intersection was a diffuse which was directly lit with multiple importance sampling,
	// then calculate light emission with multiple importance sampling as well.
	const float weightMIS = (thePrd.flags & FLAG_DIFFUSE) ? powerHeuristic(thePrd.pdf, 0.25f * M_1_PIf) : 1.0f;
	thePrd.radiance = make_float3(weightMIS); // Constant white emission multiplied by MIS weight.
#else
	thePrd.radiance = make_float3(1.0f); // Constant white emission.
#endif

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
	thePrd.albedo = make_float3(1.0f); // Constant white emission.
#endif
#endif

	thePrd.flags |= (FLAG_LIGHT | FLAG_TERMINATE);
}

RT_PROGRAM void miss_environment_mapping()
{
	const LightDefinition light = sysLightDefinitions[0];

	const float3 R = theRay.direction;

	float theta = atan2f(R.x, R.y);
	float phi = M_PIf * 0.5f - acosf(R.z);
	float u = (theta + M_PIf) * (0.5f * M_1_PIf);
	float v = 0.5f * (1.0f + sin(phi));


	// The seam u == 0.0 == 1.0 is in positive z-axis direction.
	// Compensate for the environment rotation done inside the direct lighting.
	//const float u = (atan2f(R.x, -R.z) + M_PIf) * 0.5f * M_1_PIf + sysEnvironmentRotation; // DAR FIXME Use a light.matrix to rotate the environment.
	//const float theta = acosf(-R.y);     // theta == 0.0f is south pole, theta == M_PIf is north pole.
	//const float v = theta * M_1_PIf; // Texture is with origin at lower left, v == 0.0f is south pole.

	//const bool show_sun = true;// (prd_radiance.depth == 0);
	//float3 rad = theRay.direction.z <= 0.0f ? make_float3(0.0f) : querySkyModel(show_sun, theRay.direction);

	const float3 emissionMap = make_float3(optix::rtTex2D<float4>(light.idEnvironmentTexture, u, v));
	const float3 emission = emissionMap;// + tonemap(rad);


#if USE_NEXT_EVENT_ESTIMATION
	float weightMIS = 1.0f;
	// If the last surface intersection was a diffuse event which was directly lit with multiple importance sampling,
	// then calculate light emission with multiple importance sampling for this implicit light hit as well.
	if (thePrd.flags & FLAG_DIFFUSE)
	{
		// For simplicity we pretend that we perfectly importance-sampled the actual texture-filtered environment map
		// and not the Gaussian smoothed one used to actually generate the CDFs.
		const float pdfLight = intensity(emission) / light.environmentIntegral;
		weightMIS = powerHeuristic(thePrd.pdf, pdfLight);
	}
	thePrd.radiance = emission * weightMIS;
#else
	thePrd.radiance = emission;
#endif

#if USE_DENOISER
#if USE_DENOISER_ALBEDO
	thePrd.albedo = emission;
#endif
#endif

	thePrd.flags |= (FLAG_LIGHT | FLAG_TERMINATE);
}



RT_PROGRAM void miss_sky_environment_mapping()
{
	const bool show_sun = (thePrd.depth == 0);
	float3 sunRad = querySkyModel(show_sun, theRay.direction);
	float3 rad = theRay.direction.z <= 0.0f ? make_float3(0.0f) : sunRad;

	const float weightMIS = (thePrd.flags & FLAG_DIFFUSE) ? powerHeuristic(thePrd.pdf, 0.25f * M_1_PIf) : 1.0f;
	thePrd.radiance = weightMIS * rad;

	thePrd.flags |= (FLAG_LIGHT | FLAG_TERMINATE);
}
