#pragma once

namespace TRAY
{
	enum SurfaceTextureType
	{
		DIFFUSE = 0,
		METALLIC,
		ROUGHNESS,
		SPECULAR,
		EMISSIVE,
		BUMP,
		NORMAL,

		MAX_NUM
	};

	enum eCameraType
	{
		PINHOLE = 0,
		FISHEYE = 1,
		SPHERICAL = 2,

		TOTAL_CAM_TYPE
	};




}