#pragma once

#include <vector>
#include <map>
#include <OptiXMesh.h>

#include "inc/math_utils/Vector3.h"
#include "inc/math_utils/Vector2.h"
#include "inc/math_utils/Matrix4.h"
#include "TRayNode.h"
#include <algorithm>

using namespace TRAY;

class TNode
{
public:
	TNode(TMesh* aMesh, int aMaterialIndex = 0, const Matrix4F& transformMat = Matrix4F(1.0f))
	{
		AddMesh(aMesh, aMaterialIndex);
		worldTransformMat = transformMat;
	}

	TNode(std::vector<TMesh*> aMeshList, const Matrix4F& transformMat = Matrix4F(1.0f))
	{
		for (auto i = 0; i < aMeshList.size(); ++i)
		{
			AddMesh(aMeshList[i], 0);
		}
		worldTransformMat = transformMat;
	}

	~TNode()
	{
		map<TMesh*, int> ::iterator it;
		for (it = materialIndexToMeshMap.begin(); it != materialIndexToMeshMap.end(); ++it)
		{
			delete it->first;
		}
		materialIndexToMeshMap.clear();
	}

	void AddMesh(TMesh* aMesh, int aMaterialIndex)
	{
		materialIndexToMeshMap[aMesh] = aMaterialIndex;
	}

	std::map<TMesh*, int> GetMeshToMatMap() const { return materialIndexToMeshMap; }
	Matrix4F GetWorldTransform() const { return worldTransformMat; }

private:
	std::map<TMesh*, int> materialIndexToMeshMap;
	Matrix4F			  worldTransformMat;
};

inline void scanMesh(const TRAY::TRayMesh& tMesh, Mesh& mesh)
{
	mesh.num_triangles = static_cast<int32_t>(tMesh.GetIndices().size() / 3);
	mesh.num_vertices = static_cast<int32_t>(tMesh.GetPositions().size());

	if (!tMesh.GetNormals().empty() && tMesh.GetNormals().size() == tMesh.GetPositions().size())
	{
		mesh.has_normals = true;
	}

	if (!tMesh.GetUVCoords().empty() && tMesh.GetUVCoords().size() == tMesh.GetPositions().size())
	{
		mesh.has_texcoords = true;
	}

	mesh.num_materials = 1;
}

inline void LoadMeshData(const TRAY::TRayMesh& tMesh, Mesh& mesh)
{
	auto positions = tMesh.GetPositions();
	auto normals = tMesh.GetNormals();
	auto tangents = tMesh.GetTangents();
	auto uvs = tMesh.GetUVCoords();
	auto indices = tMesh.GetIndices();

	for (int vertexIndex = 0; vertexIndex < positions.size(); vertexIndex++)
	{
		const float x = positions[vertexIndex].x;
		const float y = positions[vertexIndex].y;
		const float z = positions[vertexIndex].z;

		mesh.bbox_min[0] = std::min<float>(mesh.bbox_min[0], x);
		mesh.bbox_min[1] = std::min<float>(mesh.bbox_min[1], y);
		mesh.bbox_min[2] = std::min<float>(mesh.bbox_min[2], z);

		mesh.bbox_max[0] = std::max<float>(mesh.bbox_max[0], x);
		mesh.bbox_max[1] = std::max<float>(mesh.bbox_max[1], y);
		mesh.bbox_max[2] = std::max<float>(mesh.bbox_max[2], z);

		mesh.positions[vertexIndex * 3 + 0] = x;
		mesh.positions[vertexIndex * 3 + 1] = y;
		mesh.positions[vertexIndex * 3 + 2] = z;
	}

	if (mesh.has_normals)
	{
		for (uint64_t normalIndex = 0; normalIndex < normals.size(); ++normalIndex)
		{
			mesh.normals[normalIndex * 3 + 0] = normals[normalIndex].x;
			mesh.normals[normalIndex * 3 + 1] = normals[normalIndex].y;
			mesh.normals[normalIndex * 3 + 2] = normals[normalIndex].z;
		}
	}

	mesh.has_tangents = false;
	if (mesh.has_texcoords)
	{
		for (uint64_t textureIndex = 0; textureIndex < uvs.size(); ++textureIndex)
		{
			mesh.texcoords[2 * textureIndex + 0] = uvs[textureIndex].x;
			mesh.texcoords[2 * textureIndex + 1] = uvs[textureIndex].y;
		}
	}

	for (uint64_t i = 0; i < indices.size() / 3; ++i)
	{
		mesh.tri_indices[i * 3 + 0] = indices[i * 3 + 0];
		mesh.tri_indices[i * 3 + 1] = indices[i * 3 + 1];
		mesh.tri_indices[i * 3 + 2] = indices[i * 3 + 2];
		mesh.mat_indices[i] = 0;
	}

	if (mesh.has_texcoords)
	{
		const auto numOfTriangles = mesh.num_triangles;
		for (auto triangleIndex = 0; triangleIndex < numOfTriangles; triangleIndex++)
		{
			const auto i0 = mesh.tri_indices[triangleIndex * 3 + 0];
			const auto i1 = mesh.tri_indices[triangleIndex * 3 + 1];
			const auto i2 = mesh.tri_indices[triangleIndex * 3 + 2];

			// Vertex 0
			const float v0x = mesh.positions[3 * i0 + 0];
			const float v0y = mesh.positions[3 * i0 + 1];
			const float v0z = mesh.positions[3 * i0 + 2];

			// Vertex 1
			const float v1x = mesh.positions[3 * i1 + 0];
			const float v1y = mesh.positions[3 * i1 + 1];
			const float v1z = mesh.positions[3 * i1 + 2];

			// Vertex 2
			const float v2x = mesh.positions[3 * i2 + 0];
			const float v2y = mesh.positions[3 * i2 + 1];
			const float v2z = mesh.positions[3 * i2 + 2];

			// Vertex UV0
			const float uv0x = mesh.texcoords[2 * i0 + 0];
			const float uv0y = mesh.texcoords[2 * i0 + 1];

			// Vertex UV1
			const float uv1x = mesh.texcoords[2 * i1 + 0];
			const float uv1y = mesh.texcoords[2 * i1 + 1];

			// Vertex UV2
			const float uv2x = mesh.texcoords[2 * i2 + 0];
			const float uv2y = mesh.texcoords[2 * i2 + 1];

			// Edges of the triangle
			// Edge 01 (Position Delta)
			const float deltaPos1x = v1x - v0x;
			const float deltaPos1y = v1y - v0y;
			const float deltaPos1z = v1z - v0z;

			// Edge 02 (Position Delta)
			const float deltaPos2x = v2x - v0x;
			const float deltaPos2y = v2y - v0y;
			const float deltaPos2z = v2z - v0z;

			// Edge 01 (UV Delta)
			const float deltaUV1x = uv1x - uv0x;
			const float deltaUV1y = uv1y - uv0y;

			// Edge 02 (UV Delta)
			const float deltaUV2x = uv2x - uv0x;
			const float deltaUV2y = uv2y - uv0y;

			const float r = 1.0f / (deltaUV1x * deltaUV2y - deltaUV1y * deltaUV2x);
			const float tangentx = (deltaPos1x * deltaUV2y - deltaPos2x * deltaUV1y)*r;
			const float tangenty = (deltaPos1y * deltaUV2y - deltaPos2y * deltaUV1y)*r;
			const float tangentz = (deltaPos1z * deltaUV2y - deltaPos2z * deltaUV1y)*r;

			mesh.tangents[3 * i0 + 0] = tangentx;
			mesh.tangents[3 * i0 + 1] = tangenty;
			mesh.tangents[3 * i0 + 2] = tangentz;

			mesh.tangents[3 * i1 + 0] = tangentx;
			mesh.tangents[3 * i1 + 1] = tangenty;
			mesh.tangents[3 * i1 + 2] = tangentz;

			mesh.tangents[3 * i2 + 0] = tangentx;
			mesh.tangents[3 * i2 + 1] = tangenty;
			mesh.tangents[3 * i2 + 2] = tangentz;
		}
		mesh.has_tangents = true;
	}

	// FIX ME : Keeping single material for each mesh
	for (uint64_t i = 0; i < 1; ++i)
	{
		MaterialParams mat_params;

		mat_params.name = "Test Mat";
		mat_params.Kd_map = "";

		mat_params.Kd[0] = 1.0f;
		mat_params.Kd[1] = 1.0f;
		mat_params.Kd[2] = 1.0f;

		mat_params.Ks[0] = 0.5f;
		mat_params.Ks[1] = 0.5f;
		mat_params.Ks[2] = 0.5f;

		mat_params.Ka[0] = 0.5f;
		mat_params.Ka[1] = 0.5f;
		mat_params.Ka[2] = 0.5f;

		mat_params.Kr[0] = 0.5f;
		mat_params.Kr[1] = 0.5f;
		mat_params.Kr[2] = 0.5f;

		mat_params.exp = 0.5f;

		mesh.mat_params[i] = mat_params;
	}

}

inline void LoadMesh(const TRAY::TRayMesh& tMesh, OptiXMesh& optixMesh, const optix::Matrix4x4& load_xform)
{
	try
	{
		if (!optixMesh.context)
		{
			throw std::runtime_error("OptiXMesh: loadMesh() requires valid OptiX context");
		}

		optix::Context context = optixMesh.context;
		Mesh mesh;
		scanMesh(tMesh, mesh);

		MeshBuffers buffers;
		setupMeshLoaderInputs(context, buffers, mesh);

		// TODO : transfer data from TNode to Mesh
		mesh.bbox_min[0] = mesh.bbox_min[1] = mesh.bbox_min[2] = 1e16f;
		mesh.bbox_max[0] = mesh.bbox_max[1] = mesh.bbox_max[2] = -1e16f;

		LoadMeshData(tMesh, mesh);
		applyLoadXForm(mesh, load_xform.getData());

		translateMeshToOptiX(mesh, buffers, optixMesh);
		unmap(buffers, mesh);
	}
	catch (optix::Exception& e)
	{
		std::cerr << e.getErrorString() << "\n";
	}
}
