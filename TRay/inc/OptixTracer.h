#pragma once

#if defined(_WIN32)
#include <windows.h>
#endif

#include <imgui/imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS 1
#include <imgui/imgui_internal.h>

#include <imgui/imgui_impl_glfw_gl2.h>

#ifndef __APPLE__
#  include <GL/glew.h>
#  if defined( _WIN32 )
#    include <GL/wglew.h>
#  endif
#endif

#include <GLFW/glfw3.h>

#include <optix.h>
#include <optixu/optixpp_namespace.h>
#include <optixu_aabb_namespace.h>

#include "inc/LensShader.h"
#include "inc/PinholeCamera.h"
#include "inc/Timer.h"
#include "inc/Picture.h"
#include "inc/Texture.h"
#include "inc/TRayNode.h"
#include "inc/TRayMesh.h"

#include <TNDataBuffer.h>

#include "shaders/vertex_attributes.h"
#include "shaders/light_definition.h"
#include "shaders/material_parameter.h"

#include <string>
#include <map>
#include "SunSky.h"
#include "device_include/commonStructs.h"

#if USE_TREZI_CAMERA
#include "Camera/Camera.h"
#endif


// For rtDevice*() function error checking. No OptiX context present at that time.
#define RT_CHECK_ERROR_NO_CONTEXT( func ) \
  do { \
    RTresult code = func; \
    if (code != RT_SUCCESS) \
      std::cerr << "ERROR: Function " << #func << std::endl; \
  } while (0)

namespace TRAY 
{

	enum GuiState
	{
		GUI_STATE_NONE,
		GUI_STATE_ORBIT,
		GUI_STATE_PAN,
		GUI_STATE_DOLLY,
		GUI_STATE_FOCUS
	};

	// Host side GUI material parameters 
	struct MaterialParameterGUI
	{
		FunctionIndex		indexBSDF;  // BSDF index to use in the closest hit program
		optix::float3		albedo;     // Tint, throughput change for specular materials
		bool				useAlbedoTexture;
		bool				useCutoutTexture;
		bool				thinwalled;
		optix::float3		absorptionColor; // absorption color and distance scale together build the absorption coefficient
		float				volumeDistanceScale;
		float				ior;        // index of refraction
	};


	class OptixTracer
	{
	public:
		OptixTracer(GLFWwindow* window,
			int width,
			int height,
			unsigned int devices,
			unsigned int stackSize,
			bool interop,
			bool light,
			unsigned int miss,
			std::string const& environment, class Application* aApp);
		~OptixTracer();

		bool isValid() const;

		void reshape(int width, int height);

		bool render();
		void display();

		void screenshot(std::string const& filename);

		void guiNewFrame();
		void guiWindow();
		void guiEventHandler();
		void guiRender();

		void guiReferenceManual();
		void restartAccumulation();

#if USE_TREZI_CAMERA
		TRAY::Camera* GetCameraPtr() const
		{
			return mCamera;
#else
		PinholeCamera* GetCameraPtr() const
		{
			return m_pinholeCamera;
#endif
		}

		optix::Context GetContext();
		optix::Aabb GetSceneAABB();

		TreziNet::TNDataBufferPtr	GetRenderedBuffer(int& OutImageWidth, int& OutImageHeight);
		
		DirectionalLight&					GetSun() { return sun; }
		sutil::PreethamSunSky&				GetSky() { return sky; }
		void								PostProcessSkySunModel();
	private:
		void getSystemInformation();

		void initOpenGL();
		void checkInfoLog(const char *msg, GLuint object);
		void initGLSL();

		void initOptiX();
		void initRenderer();
		void initPrograms();
		void initMaterials();
		void initScene();

#if USE_TREZI_SAMPLE
		void InitOptixMaterials();
		void InitGeometry();
		
		//void InitNodes();

		//optix::Material CreateMaterial(const OptixMaterial &mat, int index);
		optix::Geometry CreateGeometry(const TRayMesh* const trayMesh, optix::Aabb& aabb);
		void AddGeometry(optix::Geometry& geometry, optix::Material& material, int materialID, 
                        const optix::Matrix4x4& transform);
		//optix::Aabb CreateGeometry(optix::Group& top_group);
#endif

		void createScene();
		optix::Geometry createBox();
		optix::Geometry createPlane(const int tessU, const int tessV, const int upAxis);
		optix::Geometry createSphere(const int tessU, const int tessV, const float radius, const float maxTheta);
		optix::Geometry createTorus(const int tessU, const int tessV, const float innerRadius, const float outerRadius);

		optix::Geometry createParallelogram(optix::float3 const& position, optix::float3 const& vecU, optix::float3 const& vecV, optix::float3 const& normal);

		optix::Geometry createGeometry(std::vector<VertexAttributes> const& attributes, std::vector<unsigned int> const& indices);

		void setAccelerationProperties(optix::Acceleration acceleration);

		void createLights();

		void updateMaterialParameters();

		void SetSkyVariables(sutil::PreethamSunSky::SkyVariables& sky_variables);

		
	private:
		GLFWwindow* m_window;

		int         m_width;
		int         m_height;

		bool        m_isValid;

		// OptixTracer command line parameters.
		unsigned int m_devicesEncoding;
		unsigned int m_stackSize;
		bool         m_interop;
		bool         m_light;
		unsigned int m_missID;
		std::string m_environmentFilename;

		// Applicatoin GUI parameters.
		int   m_minPathLength;       // Minimum path length after which Russian Roulette path termination starts.
		int   m_maxPathLength;       // Maximum path length.
		float m_sceneEpsilonFactor;  // Factor on 1e-7 used to offset ray origins along the path to reduce self intersections. 
		float m_environmentRotation;

		int   m_iterationIndex;

		std::string m_builder;

		// OpenGL variables:
		GLuint m_pboOutputBuffer;
		GLuint m_hdrTexture;

		// OptiX variables:
		optix::Context m_context;

		optix::Buffer m_bufferOutput;
		optix::Buffer m_bufferAccum;
		optix::Buffer light_buffer;

		std::map<std::string, optix::Program> m_mapOfPrograms;

		// The material parameters exposed inside the GUI are slightly different than the resulting values for the device.
		// The GUI exposes an absorption color and a distance scale, and the thin-walled property as bool.
		// These are converted on the fly into the device side sysMaterialParameters buffer.
		std::vector<MaterialParameterGUI> m_guiMaterialParameters;
		optix::Buffer                     m_bufferMaterialParameters; // Array of MaterialParameters.



		optix::Buffer m_bufferLensShader;
		optix::Buffer m_bufferSampleBSDF;
		optix::Buffer m_bufferEvalBSDF;
		optix::Buffer m_bufferSampleLight;

		bool   m_present; // This controls if the texture image is updated per launch or only once a second.
		bool   m_presentNext;
		double m_presentAtSecond;

		int m_frames;

		// GLSL shaders objects and program.
		GLuint m_glslVS;
		GLuint m_glslFS;
		GLuint m_glslProgram;

		// Tonemapper group:
		bool		  m_useToneMapper;
		float         m_gamma;
		optix::float3 m_colorBalance;
		float         m_whitePoint;
		float         m_burnHighlights;
		float         m_crushBlacks;
		float         m_saturation;
		float         m_brightness;

		float		  m_contrast;
		float		  m_brightnessVal;

		GuiState m_guiState;

		bool m_isWindowVisible; // Hide the GUI window completely with SPACE key.

		float m_mouseSpeedRatio;

		LensShader		m_cameraType;
		int				m_shutterType;
#if USE_TREZI_CAMERA
		TRAY::Camera*	mCamera;
#else
		PinholeCamera*	m_pinholeCamera;
#endif


		Timer m_timer;

		std::vector<LightDefinition> m_lightDefinitions;
		optix::Buffer                m_bufferLightDefinitions;

		Texture m_environmentTexture;

		Texture m_textureAlbedo;
		Texture m_textureCutout;

		// There are only three types of materials in this demo.
		// The material parameters for these are determined by the parMaterialIndex variable on the GeometryInstance.
		optix::Material m_opaqueMaterial; // Used for all materials without cutout opacity.
		optix::Material m_cutoutMaterial; // Used for all materials with cutout opacity.
		optix::Material m_lightMaterial;  // Used for all geometric lights. (Special cased diffuse emission distribution function to simplify the material system.)
		//optix::Material m_glassMaterial;

		// The root node of the OptiX scene graph (sysTopObject)
		optix::Group        m_rootGroup;
		optix::Acceleration m_rootAcceleration;


#if USE_TREZI_SAMPLE
		class TRAY::Application* mApplicationPtr;
		
		optix::Aabb							mAabb;
		std::vector<class OptixMaterial>	materialList;
		std::vector<TMesh>					tMeshList;
		std::vector<TNode*>					nodeList;

		DirectionalLight					sun;
		sutil::PreethamSunSky				sky;

#endif

#if USE_DENOISER
		optix::CommandList         m_commandListDenoiser;
		optix::PostprocessingStage m_stageDenoiser;
		optix::Buffer              m_bufferDenoised;
#if USE_DENOISER_ALBEDO
		optix::Buffer              m_bufferAlbedo;
#if USE_DENOISER_NORMAL
		optix::Buffer              m_bufferNormals; // Normals can only be used when albedo is also used.
#endif
#endif
		float                      m_denoiseBlend;
#endif
	};

}