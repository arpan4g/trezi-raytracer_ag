#pragma once

#include <string>
#include <vector>
#include <inc/math_utils/Vector2.h>
#include <inc/math_utils/Vector3.h>
#include <inc/math_utils/Vector4.h>
#include <inc/math_utils/Matrix4.h>
//#include <Utils/math/VertexAttributes.h>

#include <map>

namespace TRAY
{
	class TRayNode
	{
	public:
		TRayNode(std::string aName, bool activeState, std::vector<int> aMeshInd, const std::map<int, int>& MatInds, const Matrix4F& aTransMat)
		{
			Name = aName;
			IsActive = activeState;
			MeshIndices = aMeshInd;
			TransformMat = aTransMat;
			MaterialIndices = MatInds;
		}

		std::vector<int>				GetMeshIndices()	const { return MeshIndices; }
		std::string						GetNodeName()		const { return Name; }
		bool							GetActiveState()	const { return IsActive; }
		Matrix4F						WorldTransformMat() const { return TransformMat; }
		int								GetMaterialIndex(int SMeshInd) const
		{
			const int SMeshCount = (int)MeshIndices.size();
			if (SMeshInd >= 0)// && SMeshInd < SMeshCount)
			{
				std::map<int, int>::const_iterator itr = MaterialIndices.find(SMeshInd);
				if (itr == MaterialIndices.end())
				{
					return -1;
				}

				return itr->second;
			}

			return -1;
		}

	private:
		std::string Name;
		bool IsActive;
		std::vector<int> MeshIndices;
		std::map<int, int> MaterialIndices;
		Matrix4F TransformMat;
	};

	class TRayMesh
	{
	public:
		TRayMesh(std::string aName, const std::vector<Vector3F>& aPos, const std::vector<Vector3F>& aNormal, 
			const std::vector<Vector3F>& aTangents, const std::vector<Vector2F>& aUV, 
			const std::vector<unsigned int>& aIndices, int aMaterialIndex)
		{
			assert((aPos.size() == aNormal.size() && aPos.size() == aUV.size()) && "Number of elements in each attributes should match with vertex Number!!");
			assert(aIndices.size() >= 3 && "There should be atleast 1 face in the geometry");

			mName = aName;
			mMaterialIndex = aMaterialIndex;

			const auto numOfVert = aPos.size();
			for (auto i = 0; i < numOfVert; i++)
			{
				mPositions.emplace_back(aPos[i]);
				mNormals.emplace_back(aNormal[i]);
				mTangents.emplace_back(aTangents[i]);
				mUVCoords.emplace_back(aUV[i]);
			}
			mIndices = aIndices;
			mNumOfTriangles = mIndices.size() / 3;
		}

		TRayMesh(const std::vector<Vector3F>& aPos, const std::vector<Vector3F>& aNormal, const std::vector<Vector3F>& aTangents, const std::vector<Vector2F>& aUV, const std::vector<unsigned int>& aIndices, int aMaterialIndex)
		{
			assert((aPos.size() == aNormal.size() && aPos.size() == aUV.size()) && "Number of elements in each attributes should match with vertex Number!!");
			assert(aIndices.size() >= 3 && "There should be atleast 1 face in the geometry");

			mName = "";
			mMaterialIndex = aMaterialIndex;

			const auto numOfVert = aPos.size();
			for (auto i = 0; i < numOfVert; i++)
			{
				mPositions.emplace_back(aPos[i]);
				mNormals.emplace_back(aNormal[i]);
				mTangents.emplace_back(aTangents[i]);
				mUVCoords.emplace_back(aUV[i]);
			}
			mIndices = aIndices;
			mNumOfTriangles = mIndices.size() / 3;
		}

		const std::string&				GetName() const { return mName; }

		std::vector<Vector3F>			GetPositions()		  const { return mPositions; }
		std::vector<Vector3F>			GetNormals()		  const { return mNormals; }
		std::vector<Vector3F>			GetTangents()		  const { return mTangents; }
		std::vector<Vector2F>			GetUVCoords()		  const { return mUVCoords; }
		std::vector<unsigned int>		GetIndices()		  const { return mIndices; }
		int								GetMaterialIndex()	  const { return mMaterialIndex; }
		void							SetMaterialIndex(int MatInd) { mMaterialIndex = MatInd; }
		size_t							GetNumOfTrianglesInMesh() const{ return mNumOfTriangles; }

	private:
		std::string					    mName;
		std::vector<Vector3F>			mPositions;
		std::vector<Vector3F>			mNormals;
		std::vector<Vector3F>			mTangents;
		std::vector<Vector2F>			mUVCoords;
		size_t							mNumOfTriangles;

		std::vector<unsigned int>		mIndices;
		int							    mMaterialIndex;
	};

	typedef TRayMesh TMesh;

	class TRayMaterial
	{

		// FIX ME : make the variable as private and make getter and setter for the member variables
	public:

		TRayMaterial()
		{
			albedoTexID = 0;
			albedoColor = Vector4F(1.0f, 1.0f, 1.0f, 1.0f);

			metallicTexID = 0;
			metallic = 0.0f;

			roughnessTexID = 0;
			roughness = 1.0f;

			specularTexID = 0;
			specular = 0.0f;

			emissiveTexID = 0;
			emission = Vector4F(0.0f);

			bumpTexID = 0;
			normalTexID = 0;

			specularTint = 0.0f;
			isTranslucent = false;

			isAlbedoTexFile = true;
			isMetallicTexFile = true;
			isRoughnessTexFile = true;
			isSpecularTexFile = true;
			isEmissiveTexFile = true;
			isBumpTexFile = true;
			isNormalTexFile = true;

			isTintDiffuseEnabled = false;
			tintDiffuseColor = Vector4F(1.0f, 1.0f, 1.0f, 1.0f);
		}

		TRayMaterial(const Vector4F& albedoCol, float metal, float rness, float spec)
			: albedoColor(albedoCol)
			, metallic(metal)
			, roughness(rness)
			, specular(spec)
		{
			albedoTexID = 0;
			metallicTexID = 0;
			roughnessTexID = 0;
			specularTexID = 0;
			emissiveTexID = 0;
			bumpTexID = 0;
			normalTexID = 0;

			isAlbedoTexFile = true;
			isMetallicTexFile = true;
			isRoughnessTexFile = true;
			isSpecularTexFile = true;
			isEmissiveTexFile = true;
			isBumpTexFile = true;
			isNormalTexFile = true;
			isTintDiffuseEnabled = false;
			tintDiffuseColor = Vector4F(1.0f, 1.0f, 1.0f, 1.0f);
		}

		std::string materialName;
		bool isAlbedoTexFile;
		int albedoTexID;
		Vector4F albedoColor; // diffuse color

		bool isMetallicTexFile;
		int metallicTexID;
		float metallic;

		bool isRoughnessTexFile;
		int roughnessTexID;
		float roughness;

		bool isSpecularTexFile;
		int specularTexID;
		float specular;

		bool isEmissiveTexFile;
		int emissiveTexID;
		Vector4F emission;

		bool isBumpTexFile;
		int bumpTexID;

		bool isNormalTexFile;
		int normalTexID;

		bool isTintDiffuseEnabled;
		Vector4F tintDiffuseColor;

		float specularTint;
		bool isTranslucent;

		// TODO : Add the parameters for Normal, Bump, and Opacity
	};

	class TRayLight
	{
	public:

		TRayLight()
		{
			lightLocation.setZero();
			lightDirection = Vector3F(1.0f, 0.0f, 0.0f);
			lightColor = Vector3F(1, 1, 1);
			lightIntensity = 100.0f;
		}

		~TRayLight()
		{

		}

		Vector3F lightLocation;
		Vector3F lightDirection;
		Vector3F lightColor;
		float lightIntensity;
	};

}