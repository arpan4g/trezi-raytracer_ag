#pragma once

//#include "Utils/Macros.h"

#include "TNApplication.h"
#include "TNVectorData.h"
#include "TNString.h"
#include <map>
#include <vector>
#include <TNDataBuffer.h>
#include "inc/Application.h"
#include "TNDataBuffer.h"

using namespace TreziNet;

namespace TRAY
{
	class TRayMesh;
	class TRayNode;
	class TRayMaterial;
	class Application;

	class SnapshotRequest
	{
	public:

		SnapshotRequest():mProcessing(false){}
		~SnapshotRequest();

		typedef std::pair<int, int> TSecIndMeshInd; //Section index and MeshIndex Pair
		typedef std::vector<TSecIndMeshInd> TSecIndMeshIndArr;
		typedef std::pair<std::string, TSecIndMeshIndArr> TMeshIdMeshInds;
		typedef std::map<std::string, TSecIndMeshIndArr> TMeshIndices;

		typedef std::vector< Application::TextureBufferData> TTexturesArray;
		typedef std::map<std::string, int> TTextureIndicesMap;

		typedef std::vector<TRayMaterial*> TMaterialsArray;
		typedef std::map<std::string, int> TMaterialIndicesMap;

		void OnMessageReceived(TNMessagePtr Message);
		void OnRequestReceived(TNMessagePtr Request);
		
		bool IsRequestCompleted() const;

		void LoadScene(Application* App);

		bool IsProcessing() const { return mProcessing; }

		const TRayLight& GetLightData() const { return mLightData; }

		bool CreateImageAndSendResponse(Application* TrayApp);

		void SendRequestFeedback(float Percent);

	protected:

		struct MultiRenderData
		{
			TreziNet::TNDataBufferPtr TextureData;
			int Width;
			int Height;
		};

		int GetIndexOfTexture(const TTextureIndicesMap& TexIndices, const TNString& TextureId) const;

		TRayMesh* ConstructTrayMesh(const TNString& BaseName, unsigned int SectionInd, const TNVector3Array& Verts
			, const TNVector2Array& Uvs, const TNVector3Array& Normals, const TNVector3Array& Tangents, const TNUIntArray& Inds, int MatInd);

		template<class T> void GetBuffer(std::vector<T>& OutBuffer, const char* const ByteData, int ElementCount)
		{
			OutBuffer.clear();

			T* PosArr = (T*)ByteData;
			OutBuffer = std::vector<T>(PosArr, PosArr + ElementCount);
		}

		void GetMeshIndices(const TSecIndMeshIndArr& SectionMeshInds, std::vector<int>& OutMeshInds);


		TMeshIndices mTreziMeshsInds;
		std::vector<TRayMesh*> mTreziMeshList;
		std::vector<TRayNode*> mTreziNodesList;

		TTexturesArray mTextures;
		TTextureIndicesMap mTextureIndices;

		std::vector<TRayMaterial*> mMaterials;
		TMaterialIndicesMap mMaterialIndices;

		TNMessagePtr mSnapshotRequest;

		TRayLight mLightData;
		float mSunTheta;
		float mSunPhi;

		int mCurrentMultiRenderIndex;
		std::vector<MultiRenderData> mAllMultiRenderTextureData;

		bool mProcessing;
	};
}

