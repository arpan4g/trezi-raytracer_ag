#pragma once

//#include "Utils\Macros.h"
#include "TNApplication.h"
#include "TNVectorData.h"
#include "TNString.h"
#include <map>
#include <vector>
#include <TNDataBuffer.h>
#include "inc/Application.h"
#include "SnapshotRequest.h"

using namespace TreziNet;

namespace TRAY
{
	class TRayMesh;
	class TRayNode;
	class TRayMaterial;
	class Application;

	class RayTracingService : TNListener
	{
	public:

		const int MaxAllowedConnections = 100;

		RayTracingService(Application* App, unsigned int Port);
		virtual ~RayTracingService();

		void Update();

		virtual void OnClientConnected(TNSocket* Client) override;
		virtual void OnClientDisConnected(TNSocket* Client) override;
		virtual void OnMessageReceived(TNSocket* From, TNMessagePtr Message) override;
		virtual void OnRequestReceived(TNSocket* From, TNMessagePtr Request) override;
		virtual void OnResponseReceived(TNSocket* From, TNMessagePtr Request) override;
		virtual void OnFeedbackReceived(TNSocket* From, TNMessagePtr Request) override;

		TreziNet::TNApplication* GetTNApplication() const { return mTNApplication; }

	protected:

		typedef std::list<SnapshotRequest*> TRequestsList;
		typedef std::pair< TNSocket*, TRequestsList> TUserRequestPair;
		typedef std::map<TNSocket*, TRequestsList> TRequestsFromUsers;

		SnapshotRequest* AddNewMessageToRequestQueue(TNSocket* From, TNMessagePtr Message, bool IsRequest);
		std::pair<TNSocket*, SnapshotRequest*> GetAndRemoveRequestFromList(TRequestsFromUsers& List) const;

		TreziNet::TNApplication* mTNApplication;
		Application* mTRayApp;


		void RemoveUserFromList(TRequestsFromUsers& List, TNSocket* Socket) const;
		void RemoveRequestFromList(TRequestsFromUsers& List, TNSocket* Socket, SnapshotRequest* Request) const;
		void AddRequestToList(TRequestsFromUsers& List, TNSocket* Socket, SnapshotRequest* Request) const;

		TRequestsFromUsers mRequestsPending;
		TRequestsFromUsers mRequestsComingIn;

		std::pair<TNSocket*, SnapshotRequest*> mCurrentProcessing;

		int mRenderingFrameCount = 0;
	};
}
