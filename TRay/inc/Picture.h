 // Code in these classes is based on the ILTexLoader.h/.cpp routines inside the NVIDIA nvpro-pipeline ILTexLoader plugin:
 // https://github.com/nvpro-pipeline/pipeline/blob/master/dp/sg/io/IL/Loader/ILTexLoader.cpp

#pragma once

#ifndef PICTURE_H
#define PICTURE_H

#include <string>
#include <vector>

struct Image
{
	Image();
	Image(const Image& image);
	Image(unsigned int width, unsigned int height, unsigned int depth, int format, int type);
	~Image();

	unsigned int m_width;
	unsigned int m_height;
	unsigned int m_depth;

	int          m_format; // DevIL image format.
	int          m_type;   // DevIL image component type.

	// Derived values.
	unsigned int m_bpp; // bytes per pixel
	unsigned int m_bpl; // bytes per scanline
	unsigned int m_bps; // bytes per slice (plane)
	unsigned int m_nob; // number of bytes (complete image)

	unsigned char* m_pixels; // The pixel data of one image.
};


class Picture
{
public:
	Picture();
	Picture(const Picture& pic);
	Picture(const Picture* pic);
	~Picture();

	bool load(const std::string& filename);
	void clear();

	unsigned int getNumberOfImages() const;
	unsigned int getNumberOfFaces(unsigned int indexImage) const;
	const Image* getImageFace(unsigned int indexImage, unsigned int indexFace) const;
	Image* getImageFace(unsigned int indexImage, unsigned int indexFace);
	bool isCubemap() const;

	unsigned int addImage(const Image& Img);
	unsigned int addImage(unsigned int width, unsigned int height, unsigned int depth, int format, int type);
	bool copyMipmaps(unsigned int index, std::vector<const void*> const& mipmaps);
	void setImageData(unsigned int index, const void* pixels, std::vector<const void*> const& mipmaps);
	void mirrorX(unsigned int index);
	void mirrorY(unsigned int index);

private:
	bool m_isCube; // Track if the picture is a cube map.
	std::vector< std::vector<Image> > m_images;
};

#endif // PICTURE_H
