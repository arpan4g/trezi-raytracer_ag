#pragma once

#include "shaders\rt_function.h"

#include <optix.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>

namespace TRAY
{

	enum eBrdfType
	{
		DISNEY,
		GLASS,
		LAMBERT,
		DSPBR,

		TOTAL_BRDF_TYPE
	};


	struct OptixMaterial
	{
		RT_FUNCTION OptixMaterial()
		{
			color = optix::make_float3(1.0f, 1.0f, 1.0f);
			emission = optix::make_float3(0.0f);
			metallic = 0.0f;
			subsurface = 0.0f;
			specular = 0.0f;
			roughness = 1.0f;
			specularTint = 0.0f;
			anisotropic = 0.0f;
			sheen = 0.0f;
			sheenTint = 0.0f;
			clearcoat = 0.0f;
			clearcoatGloss = 0.0f;
			brdf = DISNEY;

			albedoTexID = RT_TEXTURE_ID_NULL;
			metallicTexID = RT_TEXTURE_ID_NULL;
			roughnessTexID = RT_TEXTURE_ID_NULL;
			normalTexID = RT_TEXTURE_ID_NULL;
			bumpTexID = RT_TEXTURE_ID_NULL;
			specularTexID = RT_TEXTURE_ID_NULL;
			emissiveTexID = RT_TEXTURE_ID_NULL;
		}

		int albedoTexID;
		int metallicTexID;
		int roughnessTexID;
		int normalTexID;
		int bumpTexID;
		int specularTexID;
		int emissiveTexID;

		optix::float3 color;
		optix::float3 emission;
		float metallic;
		float subsurface;
		float specular;
		float roughness;
		float specularTint;
		float anisotropic;
		float sheen;
		float sheenTint;
		float clearcoat;
		float clearcoatGloss;
		eBrdfType brdf;
	};
}