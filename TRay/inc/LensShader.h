#pragma once

#ifndef LENS_SHADER_H
#define LENS_SHADER_H

enum LensShader
{
  LENS_SHADER_PINHOLE  = 0,
  LENS_SHADER_FISHEYE  = 1,
  LENS_SHADER_SPHERE   = 2
};

#endif // LENS_SHADER_H
