#pragma once

#include <TreziSceneGraphSDK.h>
#include <TNDataBuffer.h>
#include <inc/OptixTracer.h>
#include <inc/Camera/Camera.h>
#include <inc/TRayNode.h>
#include <inc/EnumTypes.h>

namespace TRAY 
{
	class Application
	{
	public:
		static std::string Application::TZI_FILE_PATH;

		struct TextureBufferData
		{
			int Width;
			int Height;
			TreziNet::TNDataBufferPtr Data;
		};

		Application(GLFWwindow* window, const char* const aFileName, int aWidth, int aHeight, std::string const& environment);
		~Application();

		bool									LoadAssetsFromTziFile(const char* const aFileName);
		void									RenderGUIFrame();
		void									Display();
		void									Render();
		

#if USE_TREZI_CAMERA
		void									SetCamera(class TRAY::Camera* aCam);
		class TRAY::Camera*						GetCamera() const;
#else
		void									SetCamera(class PinholeCamera* aCam);
		class PinholeCamera*					GetCamera() const;
#endif
		//eCameraType							GetCameraType() const { return mCameraPtr->GetCamType(); }

		void									CleanUpScene();
		void									InitTracer();

		//TRAY::Window*							GetWindow() const;
		TRAY::OptixTracer*						GetTracer() const;
		void									ReshapeBufferOutput(int aNewWidth, int aNewHeight);

		size_t									GetNumOfTriangle();
		std::vector<TRayMesh*>					GetMeshList()	   const { return  mTreziMeshsList; };
		std::vector<TRayMaterial*>				GetMaterialList()  const { return mTreziMaterialsList; };
		std::vector<TRayNode*>					GetNodeList()      const { return mTreziNodesList; };
		std::map<int, std::string>				GetTextureMaps()   const { return  mTextureMaps; }
		const char*								GetTextureBuffer(const TRayMaterial* Mat, SurfaceTextureType Surface, 
																int& OutBufferSize, int& OutWidth, int& OutHeight) const;

		void									AddTRayMesh(TRayMesh* aMesh);
		void									AddTRayMaterial(TRayMaterial* aMat);
		void									AddTRayNode(TRayNode* aNode);

		void									SetTRayMeshList(const std::vector<TRayMesh*>& aMeshList);
		void									SetTRayMaterialList(const std::vector<TRayMaterial*>& aMatList);
		void									SetTRayNodeList(const std::vector<TRayNode*>& aNodeList);
		void									SetTRayTextureBuffersList(const std::map<int, TextureBufferData>& TextureBuffers);
		void									SetSunAngles(float theta, float phi);

		// Removes the nodes does not have any mesh in it;
		void									CleanUpEmptyNodeList();
		void									SaveScreenShot(std::string const& fileName);

		void									Update(float DeltaTime);
		bool									IsAppRunning() const;
		bool									IsRenderingPaused() const { return mPauseRendering; }
		void									ExitApp() { mIsRunning = false; }
		void									SetRenderingState(bool state) { mPauseRendering = state; }

	private:

		void									GenerateNodes(TreziSceneGraph::TreziScene* Scene, TreziSceneGraph::TreziNode* aRootNode);
		void									ProcessNodes(TreziSceneGraph::TreziScene* Scene, std::vector<Matrix4F>& aTransformStack, 
																									TreziSceneGraph::TreziNode* aNode);

	private:

#if USE_TREZI_CAMERA
		TRAY::Camera*							mCameraPtr;
#else
		class PinholeCamera*					mCameraPtr;
#endif
		//TRAY::Window*							mWindowPtr;
		GLFWwindow*								mWindow;
		TRAY::OptixTracer*						mTracerPtr;
		class RayTracingService*				mRayTracingService;

		size_t									mNumOfTriangles;
		bool									isNumOfTrianglesComputed;
		std::vector<TRayMesh*>					mTreziMeshsList;
		std::vector<TRayMaterial*>				mTreziMaterialsList;
		std::vector<TRayNode*>					mTreziNodesList;
		std::map<int, std::string>				mTextureMaps;
		std::map<int, TextureBufferData>		mTextureBuffers;

		bool									mIsSceneDirty;
		bool									mIsRunning;
		bool									mPauseRendering;

		unsigned int							mNumOfDevices;
		unsigned int							mStackSize;
		std::string								mEnvironmentMapPath;

	};
}
