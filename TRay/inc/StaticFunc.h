#pragma once

#include <string>
#include <sutil.h>

#include "optixu/optixu_matrix_namespace.h"
#include "TreziSceneGraphSDK.h"
#include "inc/math_utils/Matrix4.h"

using namespace optix;
using namespace TRAY;




static void error_callback(int error, const char* description)
{
	std::cerr << "Error: " << error << ": " << description << std::endl;
}


static optix::Matrix4x4 CastToOptixMatrix4x4(const Matrix4F& mat)
{
	// Matrix4x4 store in row-major form.
	// Matrix4F store in column major form.
	optix::Matrix4x4 resultMat;
	// Check for bug in order conflicts in Matrix4F and Matrix4x4

	auto modMat = mat;
#if DEBUG_SCALE
	auto scaleMat = Matrix4F(0.1f);
	modMat = scaleMat * modMat;
#endif


	for (auto i = 0; i < 16; i++)
	{
		resultMat[i] = modMat.elements[i];
	}
	return resultMat.transpose();
}

static Matrix4F CastToMatrix4F(const optix::Matrix4x4& mat)
{
	Matrix4F resultMat;
	for (auto i = 0; i < 16; i++)
	{
		resultMat.elements[i] = mat[i];
	}
	resultMat = resultMat.transpose();
	return resultMat;
}

static Matrix4F CastToMatrix4F(const TreziSceneGraph::TreziMat16& mat)
{
	Matrix4F resultMat;
	for (auto i = 0; i < 16; i++)
	{
		resultMat.elements[i] = mat.mat16[i];
	}
	//resultMat = resultMat.transpose();
	return resultMat;
}

static optix::float3 make_float3(const TRAY::Vector3F& vec) 
{
	return optix::make_float3(vec.x, vec.y, vec.z);
}

static optix::float3 make_float3(const TRAY::Vector2F& vec, float val = 0.0f)
{
	return optix::make_float3(vec.x, vec.y, val);
}

static optix::GeometryInstance createSphere(optix::Context context, optix::Material material, float3 center, float radius)
{
	optix::Geometry sphere = context->createGeometry();
	sphere->setPrimitiveCount(1u);
	//TODO:
	const std::string ptx_path = "";// ptxPath("sphere_intersect.cu");
	sphere->setBoundingBoxProgram(context->createProgramFromPTXFile(ptx_path, "bounds"));
	sphere->setIntersectionProgram(context->createProgramFromPTXFile(ptx_path, "sphere_intersect_robust"));

	sphere["center"]->setFloat(center);
	sphere["radius"]->setFloat(radius);

	optix::GeometryInstance instance = context->createGeometryInstance(sphere, &material, &material + 1);
	return instance;
}

static optix::GeometryInstance createQuad(optix::Context context, optix::Material material, float3 v1, float3 v2, float3 anchor, float3 n)
{
	optix::Geometry quad = context->createGeometry();
	quad->setPrimitiveCount(1u);
	//TODO:
	const std::string ptx_path = ""; //ptxPath("quad_intersect.cu");
	quad->setBoundingBoxProgram(context->createProgramFromPTXFile(ptx_path, "bounds"));
	quad->setIntersectionProgram(context->createProgramFromPTXFile(ptx_path, "intersect"));

	float3 normal = normalize(cross(v1, v2));
	float4 plane = make_float4(normal, dot(normal, anchor));
	v1 *= 1.0f / dot(v1, v1);
	v2 *= 1.0f / dot(v2, v2);
	quad["v1"]->setFloat(v1);
	quad["v2"]->setFloat(v2);
	quad["anchor"]->setFloat(anchor);
	quad["plane"]->setFloat(plane);

	optix::GeometryInstance instance = context->createGeometryInstance(quad, &material, &material + 1);
	return instance;
}
