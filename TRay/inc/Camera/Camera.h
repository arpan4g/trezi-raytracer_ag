#pragma once

#include "inc/math_utils/Vector3.h"
#include "inc/math_utils/Matrix4.h"
#include "inc/EnumTypes.h"

namespace TRAY
{
	class Camera
	{
	public:
		Camera();
		Camera( unsigned int aWidth, unsigned int aHeight, const Vector3F& aCameraEye, const Vector3F& aCameraLookat, const Vector3F& aCameraUP, eCameraType aCamType = eCameraType::PINHOLE);

		void ResetLookAt();
		void SetSanpshotPoint();
		bool ProcessMouse( float x, float y, bool left_button_down, bool right_button_down, bool middle_button_down );
		bool Resize( unsigned int aWidth, unsigned int aHeight);

		void MoveForward(float speed);
		void MoveRight(float speed);
		void MoveUp(float speed);
		void PrintCamData() const;

		unsigned int width() const  { return mWidth; }
		unsigned int height() const { return mHeight; }
		eCameraType GetCamType() const { return mCamType; }

		Vector3F GetCameraPos() const { return mCameraEye; }
		Vector3F GetCameraLookAt() const { return mCameraLookAt; }
		Matrix4F GetCameraTransformMatrix() const;
		bool GetCameraCoordinateFrame(Vector3F& oU, Vector3F& oV, Vector3F& oW, Vector3F& oEyePos);

		void SetCameraData(const Matrix4F& aCamTransMat, float aFov, float aWidth, float aHeight, eCameraType aCamType);
		void SetCameraData(const Vector3F& aCamEye, const Vector3F& aCamLookAt, const Vector3F& aUp);

	private:
		void ComputeUVW();
		void CalculateCameraVariables( const Vector3F& aEye, const Vector3F& aLookAt, const Vector3F& aUp, 
										float  aFov, float  aAspectRatio, bool aFovIsVertical, 
										Vector3F& U, Vector3F& V, Vector3F& W) const;

		unsigned int mWidth;
		unsigned int mHeight;

		float mFov;
		eCameraType mCamType;

		Vector3F mCameraEye;
		Vector3F mCameraLookAt;
		Vector3F mCameraUP;

		Vector3F mCameraU;
		Vector3F mCameraV;
		Vector3F mCameraW;
		Matrix4F mCameraTransform;

		Vector3F mPrevCameraU;
		Vector3F mPrevCameraV;
		Vector3F mPrevCameraW;
		Vector3F mPrevCameraEye;

		Vector3F mInitialLookAtPos;
		bool isCameraChanged;
	};
}

