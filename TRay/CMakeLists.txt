OPTIX_add_sample_executable( TRay
  src/main.cpp

  inc/Application.h
  src/Application.cpp
  
  inc/OptixTracer.h
  src/OptixTracer.cpp

  src/Box.cpp
  src/Parallelogram.cpp
  src/Plane.cpp
  src/Sphere.cpp
  src/Torus.cpp

  inc/LensShader.h

  inc/PinholeCamera.h
  src/PinholeCamera.cpp

  inc/Picture.h
  src/Picture.cpp

  inc/Texture.h
  src/Texture.cpp

  inc/Timer.h
  src/Timer.cpp

  inc/MyAssert.h
  inc/EnumTypes.h
  inc/StaticFunc.h
  
  inc/TRayMesh.h
  inc/TRayNode.h
  
  inc/OptixMaterial.h
  
  inc/math_utils/Matrix4.h
  inc/math_utils/Vector2.h
  inc/math_utils/Vector3.h
  inc/math_utils/Vector4.h

  inc/Camera/Camera.h
  src/Camera/Camera.cpp
  
  inc/RayTracingService/RayTracingService.h
  src/RayTracingService/RayTracingService.cpp
  inc/RayTracingService/SnapshotRequest.h
  src/RayTracingService/SnapshotRequest.cpp
  
  shaders/app_config.h
  shaders/function_indices.h
  shaders/per_ray_data.h
  shaders/material_parameter.h
  shaders/random_number_generators.h
  shaders/light_definition.h
  shaders/rt_assert.h
  shaders/rt_function.h
  shaders/shader_common.h
  shaders/vertex_attributes.h

  shaders/boundingbox_triangle_indexed.cu
  shaders/intersection_triangle_indexed.cu

  shaders/closesthit.cu
  shaders/closesthit_light.cu
  shaders/anyhit.cu

  shaders/raygeneration.cu
  shaders/exception.cu
  shaders/miss.cu

  shaders/lens_shader.cu
  shaders/bsdf_diffuse_reflection.cu
  shaders/bsdf_specular_reflection.cu
  shaders/bsdf_specular_reflection_transmission.cu
  shaders/light_sample.cu
  shaders/disney.cu
  
  inc/Json/json.hpp
)    

include_directories(
  "."
  ${IL_INCLUDE_DIR}
)

target_link_libraries( TRay
  ${IL_LIBRARIES}
  ${ILU_LIBRARIES}
  ${ILUT_LIBRARIES}
)

