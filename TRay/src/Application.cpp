#pragma once
#include "inc\Application.h"

#include <sutil.h>
#include "inc/RayTracingService/RayTracingService.h"
#include "inc/StaticFunc.h"
#include <IL/il.h>

using namespace TreziSceneGraph;

bool interop = true;
bool light = false; // Add a geometric are light. Best used with miss 0 and 1.
int  miss = 3;     // Select the environment light (0 = black, no light; 1 = constant white environment; 3 = spherical environment texture.

namespace TRAY
{
	std::string Application::TZI_FILE_PATH = "Advanced_Material4Spheres_TZI_3D";

	Application::Application(GLFWwindow* window, const char* const aFileName, int aWidth, int aHeight, std::string const& environment)
		: mWindow(window),
		mNumOfDevices(3210),
		mStackSize(1024),
		mEnvironmentMapPath(std::string(sutil::samplesDir()) + "/resources/envmaps/" + environment),
		mIsRunning(true),
		mPauseRendering(false),
		mNumOfTriangles(0),
		isNumOfTrianglesComputed(false)
	{
		Application::TZI_FILE_PATH = aFileName;

		//auto assetPath = std::string(sutil::samplesDir()) + "/resources/Assetsfiles/" + "Trezi/housing+complex" + "/housing+complex" + ".json";
		auto assetPath = std::string(sutil::samplesDir()) + "/resources/Assetsfiles/" + TZI_FILE_PATH + ".tzi";
		LoadAssetsFromTziFile(assetPath.c_str());




		mTracerPtr = new OptixTracer(mWindow, aWidth, aHeight, mNumOfDevices, mStackSize, interop, light, miss, mEnvironmentMapPath, this);
		if (!mTracerPtr->isValid())
		{
			error_callback(4, "Optix Tracer initialization failed.");
			ilShutDown();
			glfwTerminate();
			return;
		}

		try
		{
			mCameraPtr = mTracerPtr->GetCameraPtr();
			// TODO: Port value should come through command line argument or some config file.
			mRayTracingService = new RayTracingService(this, 50000);

		}
		catch (optix::Exception& e)
		{
			std::cerr << "Error String : " << e.getErrorString() << "\n";
		}
	}

	Application::~Application()
	{
		if (mTracerPtr != nullptr)
		{
			delete mTracerPtr;
			mTracerPtr = nullptr;
		}

		for (vector<TRayMesh*>::iterator it = mTreziMeshsList.begin(); it < mTreziMeshsList.end(); ++it)
		{
			if (*it)
			{
				delete *it;
				*it = nullptr;
			}
		}

		for (vector<TRayMaterial*>::iterator it = mTreziMaterialsList.begin(); it < mTreziMaterialsList.end(); ++it)
		{
			if (*it)
			{
				delete *it;
				*it = nullptr;
			}
		}

		for (vector<TRayNode*>::iterator it = mTreziNodesList.begin(); it < mTreziNodesList.end(); ++it)
		{
			if (*it)
			{
				delete *it;
				*it = nullptr;
			}
		}

		if (mRayTracingService)
		{
			delete mRayTracingService;
			mRayTracingService = nullptr;
		}
	}

	bool Application::LoadAssetsFromTziFile(const char * const aFileName)
	{
		// TODO: do exception handling for the parsing
		bool bIsFileLoaded = false;

		TreziScene * treziScene = GetTziScene(aFileName);
		//TreziScene * treziScene = GetScene(aFileName);

		if (treziScene == nullptr)
		{
			cerr << "ERROR!! Can't parse the TZI file. \n";
			return bIsFileLoaded;
		}

		for (auto i = 0; i < treziScene->NumMeshes; i++)
		{
			TreziMesh* mesh = (treziScene->Meshes[i]);

			std::vector<Vector3F> vertexPos;
			std::vector<Vector3F> vertexNorm;
			std::vector<Vector2F> vertexUV;
			std::vector<unsigned int> indices;

			for (unsigned int vertexIndex = 0; vertexIndex < mesh->NumVertices; vertexIndex++)
			{
				const auto vertPos = mesh->Vertices[vertexIndex];
				auto pos = Vector3F(vertPos.x, vertPos.y, vertPos.z);

				const auto vertNor = mesh->Normals[vertexIndex];
				auto normal = Vector3F(vertNor.x, vertNor.y, vertNor.z);

				vertexPos.emplace_back(pos + normal * 0.001f);
				vertexNorm.emplace_back(normal);
				
				auto uv = Vector2F(1.0, 1.0);
				if (mesh->UV0) {
					const auto vertUV = mesh->UV0[vertexIndex];
					uv = Vector2F(vertUV.x, vertUV.y);
				}
				vertexUV.emplace_back(uv);
			}

			for (unsigned int index = 0; index < mesh->NumIndices; index++)
			{
				indices.emplace_back(mesh->Indices[index]);
			}

			std::vector<Vector3F> vertexTangents(vertexPos.size());
			std::vector<Vector3F> vertexBiTangents(vertexPos.size());

			for (unsigned int indexID = 0; indexID < indices.size(); indexID += 3)
			{
				const unsigned int i0 = indices[indexID];
				const unsigned int i1 = indices[indexID + 1];
				const unsigned int i2 = indices[indexID + 2];

				const Vector3F& v0Pos = vertexPos[i0];
				const Vector3F& v1Pos = vertexPos[i1];
				const Vector3F& v2Pos = vertexPos[i2];

				const Vector2F& v0Tex = vertexUV[i0];
				const Vector2F& v1Tex = vertexUV[i1];
				const Vector2F& v2Tex = vertexUV[i2];

				const Vector3F edge1 = v1Pos - v0Pos;
				const Vector3F edge2 = v2Pos - v0Pos;

				const float deltaU1 = v1Tex.x - v0Tex.x;
				const float deltaU2 = v2Tex.x - v0Tex.x;
				const float deltaV1 = v1Tex.y - v0Tex.y;
				const float deltaV2 = v2Tex.y - v0Tex.y;

				const float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
				Vector3F tangent;

				tangent.x = f * (deltaV2 * edge1.x - deltaV1 * edge2.x);
				tangent.y = f * (deltaV2 * edge1.y - deltaV1 * edge2.y);
				tangent.z = f * (deltaV2 * edge1.z - deltaV1 * edge2.z);

				vertexTangents[i0] += tangent;
				vertexTangents[i1] += tangent;
				vertexTangents[i2] += tangent;
			}

			// Normalizing the tangents
			for (unsigned int index = 0; index < vertexTangents.size(); ++index)
			{
				if (vertexTangents[index].squaredLength() > 0.0f)
					vertexTangents[index].normalize();
			}

			TRayMesh* trayMesh = new TRayMesh(mesh->Name, vertexPos, vertexNorm, vertexTangents, vertexUV, indices, mesh->MaterialIndex);
			mTreziMeshsList.emplace_back(trayMesh);
		}

		// Loading Materials
		for (auto materialIndex = 0; materialIndex < treziScene->NumMaterials; materialIndex++)
		{
			TreziMaterial* material = (treziScene->Materials[materialIndex]);
			TRayMaterial * trayMaterial = new TRayMaterial();
			trayMaterial->materialName = material->Name;
			trayMaterial->isTranslucent = material->bIsTranslucent;
			trayMaterial->isTintDiffuseEnabled = material->IsTintEnabled;

			// FIX ME : Same problem as specular value (i.e. in Disney BRDF model specularTint is float value whereas In Trezi, it is used as color value)
			// For testing I'm giving a fixed value;
			trayMaterial->specularTint = 0.5f;


			// NOTE : For now we support 5 types of texture maps (Diffuse(0), Metallic(1), Roughness(2), Specular(3), Emissive(4)) all are in order. 
			const auto NUM_OF_SURFACE_PROP = SurfaceTextureType::MAX_NUM;
			for (auto surfacePropertyIndex = 0; surfacePropertyIndex < material->NumSurfaceProperties; surfacePropertyIndex++)
			{
				auto surfaceProperty = material->SurfaceProperties[surfacePropertyIndex];
				switch (surfaceProperty->type)
				{
				case TreziSurfacePropertyType_Diffuse:
				{
					const auto color = surfaceProperty->Color;
					trayMaterial->albedoColor = Vector4F(color.r, color.g, color.b, color.a);
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::DIFFUSE, surfaceProperty->Map->Path));
					}
				}
				break;

				case TreziSurfacePropertyType_Metallic:

					trayMaterial->metallic = surfaceProperty->Value;
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::METALLIC, surfaceProperty->Map->Path));
					}

					break;

				case TreziSurfacePropertyType_Rough:

					trayMaterial->roughness = surfaceProperty->Value;
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::ROUGHNESS, surfaceProperty->Map->Path));
					}

					break;

				case TreziSurfacePropertyType_Specular:

					// For specular we need single value, so taking the alpha value for specular
					trayMaterial->specular = surfaceProperty->Color.a;
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::SPECULAR, surfaceProperty->Map->Path));
					}

					break;

				case TreziSurfacePropertyType_Emissive:
				{
					const auto color = surfaceProperty->Color;
					trayMaterial->emission = Vector4F(color.r, color.g, color.b, color.a);
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::EMISSIVE, surfaceProperty->Map->Path));
					}
				}
				break;

				case TreziSurfacePropertyType_Bump:
				{
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::BUMP, surfaceProperty->Map->Path));
					}
				}
				break;

				case TreziSurfacePropertyType_Normal:
				{
					if (surfaceProperty->Map != nullptr)
					{
						mTextureMaps.insert(make_pair(NUM_OF_SURFACE_PROP * materialIndex + SurfaceTextureType::NORMAL, surfaceProperty->Map->Path));
					}
				}
				break;
				case TreziSurfacePropertyType_Tint:
				{
					const auto color = surfaceProperty->Color;
					trayMaterial->tintDiffuseColor = Vector4F(color.r, color.g, color.b, color.a);
				}
				break;

				default:
					break;
				}
			}

			mTreziMaterialsList.emplace_back(trayMaterial);
		}

		GenerateAllTreziNodesWorldTransform(treziScene);

		GenerateNodes(treziScene, treziScene->RootNode);
		bIsFileLoaded = true;
		mIsSceneDirty = false;
		//DestroyScene(treziScene);
		cout << "Num of Triangle : " << GetNumOfTriangle() << "\n";
		return bIsFileLoaded;

	}

	void Application::RenderGUIFrame()
	{
		if (mTracerPtr)
		{
			mTracerPtr->guiNewFrame();
			mTracerPtr->guiWindow();
			mTracerPtr->guiEventHandler();
		}
	}

	void Application::Display()
	{
		if (mTracerPtr)
		{
			mTracerPtr->display();
			mTracerPtr->guiRender();
		}
	}

	void Application::Render()
	{
		if (mPauseRendering)
			return;

		if (mTracerPtr)
			mTracerPtr->render();
	}

#if USE_TREZI_CAMERA
	void Application::SetCamera(TRAY::Camera * aCam)
	{
		mCameraPtr = aCam;
	}

	TRAY::Camera * Application::GetCamera() const
	{
		return mCameraPtr;
	}

#else
	void Application::SetCamera(PinholeCamera * aCam)
	{
		mCameraPtr = aCam;
	}

	PinholeCamera * Application::GetCamera() const
	{
		return mCameraPtr;
	}
#endif

	void Application::CleanUpScene()
	{
		for (auto& trayMat : mTreziMaterialsList)
		{
			delete trayMat;
		}

		for (auto& trayMesh : mTreziMeshsList)
		{
			delete trayMesh;
		}

		for (auto& trayNode : mTreziNodesList)
		{
			delete trayNode;
		}

		mTreziMaterialsList.clear();
		mTreziMeshsList.clear();
		mTreziNodesList.clear();
		mTextureMaps.clear();
		mTextureBuffers.clear();

		if (mTracerPtr)
		{
			delete mTracerPtr;
			mTracerPtr = nullptr;
		}

		mNumOfTriangles = 0;
		isNumOfTrianglesComputed = false;
	}

	OptixTracer* Application::GetTracer() const
	{
		return mTracerPtr;
	}

	void Application::ReshapeBufferOutput(int aNewWidth, int aNewHeight)
	{
		if (mTracerPtr)
		{
			mTracerPtr->reshape(aNewWidth, aNewHeight);
		}
	}

	size_t Application::GetNumOfTriangle()
	{
		if (!isNumOfTrianglesComputed) 
		{
			for (auto& mesh : mTreziMeshsList)
				mNumOfTriangles += mesh->GetNumOfTrianglesInMesh();
		
			isNumOfTrianglesComputed = true;
			cout << "Num of Triangles in Scene : " << mNumOfTriangles << "\n";
		}

		return mNumOfTriangles;
	}

	void Application::AddTRayMesh(TRayMesh * aMesh)
	{
		mTreziMeshsList.emplace_back(aMesh);
		mIsSceneDirty = true;
	}

	void Application::AddTRayMaterial(TRayMaterial * aMat)
	{
		mTreziMaterialsList.emplace_back(aMat);
		mIsSceneDirty = true;
	}

	void Application::AddTRayNode(TRayNode * aNode)
	{
		mTreziNodesList.emplace_back(aNode);
		mIsSceneDirty = true;
	}

	void Application::SetTRayMeshList(const std::vector<TRayMesh*>& aMeshList)
	{
		mTreziMeshsList = aMeshList;
	}

	void Application::SetTRayMaterialList(const std::vector<TRayMaterial*>& aMatList)
	{
		mTreziMaterialsList = aMatList;
	}

	void Application::SetTRayNodeList(const std::vector<TRayNode*>& aNodeList)
	{
		mTreziNodesList = aNodeList;
	}

	void Application::SetTRayTextureBuffersList(const std::map<int, TextureBufferData>& TextureBuffers)
	{
		mTextureBuffers = TextureBuffers;
	}

	void Application::SetSunAngles(float theta, float phi)
	{
		if (mTracerPtr) 
		{
			bool sun_changed = false;
			auto& sky = mTracerPtr->GetSky();
			auto& sun = mTracerPtr->GetSun();

			sky.setSunPhi(M_PIf + phi);
			sky.setSunTheta(/*0.5f*M_PIf -*/ theta);

			mTracerPtr->PostProcessSkySunModel();
		}
	}

	void Application::InitTracer()
	{
		if (mWindow != nullptr)
		{
			try
			{
				int width; //= mWindowPtr->GetWidth();
				int height; //= mWindowPtr->GetHeight();
				glfwGetWindowSize(mWindow, &width, &height);
				if (mTracerPtr)
					delete mTracerPtr;

				mTracerPtr = new OptixTracer(mWindow, width, height, mNumOfDevices, mStackSize,
					interop, light, miss, mEnvironmentMapPath, this);
				auto context = mTracerPtr->GetContext();
				context->validate();
				/*const optix::Aabb aabb = mTracerPtr->GetSceneAABB();

				const optix::float3 aabb_extents = optix::make_float3(0.0f, 1.5f*aabb.extent(1), 1.5f*aabb.extent(2));
				const optix::float3 aabb_center = aabb.center();

				const Vector3F camera_eye(aabb_extents.x, aabb_extents.y, aabb_extents.z);
				const Vector3F camera_lookat(aabb_center.x, aabb_center.y, aabb_center.z);
				const Vector3F camera_up(0.0f, 1.0f, 0.0f);
				//mCameraPtr = new TRAY::Camera(width, height, camera_eye, camera_lookat, camera_up, eCameraType::PINHOLE);*/
				mCameraPtr = mTracerPtr->GetCameraPtr();



			}
			catch (optix::Exception& e)
			{
				std::cerr << "Error String : " << e.getErrorString() << "\n";
			}
		}

	}

	void Application::GenerateNodes(TreziScene* Scene, TreziNode * aRootNode)
	{
		std::vector<Matrix4F> rootTransformStack;
		const auto numOfChildrenInRoot = aRootNode->NumChildren;
		rootTransformStack.push_back(CastToMatrix4F(aRootNode->TransformMat));

		std::map<int, int> materialMap;
		std::vector<int> meshIndices;

		for (auto index = 0; index < aRootNode->NumMeshes; index++)
		{
			int meshIndex = aRootNode->MeshIndices[index];
			meshIndices.emplace_back(meshIndex);
			int materialIndex = mTreziMeshsList[meshIndex]->GetMaterialIndex();
			materialMap.insert(std::pair<int, int>(meshIndex, materialIndex));
		}

		TreziMat16 WorldTransform;
		GetTreziNodeWorldTransform(Scene, aRootNode, WorldTransform);

		TRayNode* rootNode = new TRayNode(aRootNode->Name, aRootNode->bIsActive, meshIndices, materialMap, CastToMatrix4F(WorldTransform));
		mTreziNodesList.emplace_back(rootNode);

		for (auto i = 0; i < numOfChildrenInRoot; i++)
		{
			ProcessNodes(Scene, rootTransformStack, aRootNode->Children[i]);
		}

		CleanUpEmptyNodeList();
	}

	// Recursively Get the trezi nodes and there transforms and create new Tray Node and add it into the list with there world transform. 
	void Application::ProcessNodes(TreziScene* Scene, std::vector<Matrix4F>& aTransformStack, TreziNode * aNode)
	{
		const Matrix4F nodeTransform = CastToMatrix4F(aNode->TransformMat);
		aTransformStack.push_back(nodeTransform);

		for (auto index = 0; index < aNode->NumChildren; index++)
		{
			auto node = aNode->Children[index];
			ProcessNodes(Scene, aTransformStack, node);
		}

		// FIX ME : Check for the aNode->TransformMat stores the world or local matrix.
		TreziMat16 WorldTransform;
		GetTreziNodeWorldTransform(Scene, aNode, WorldTransform);
		Matrix4F worldTransform = CastToMatrix4F(WorldTransform);
		/*for (auto i = aTransformStack.size(); i > 0; i--)
		{
			worldTransform *= aTransformStack[i - 1];
		}*/

		std::map<int, int> materialMap;
		std::vector<int> meshIndices;
		for (auto index = 0; index < aNode->NumMeshes; index++)
		{
			int meshIndex = aNode->MeshIndices[index];
			meshIndices.emplace_back(meshIndex);
			int materialIndex = mTreziMeshsList[meshIndex]->GetMaterialIndex();
			materialMap.insert(std::pair<int, int>(meshIndex, materialIndex));
		}

		TRayNode* newNode = new TRayNode(aNode->Name, aNode->bIsActive, meshIndices, materialMap, worldTransform);
		mTreziNodesList.emplace_back(newNode);
		aTransformStack.pop_back();
	}

	// Removing the nodes from the Node List which dont have any mesh in it.
	void Application::CleanUpEmptyNodeList()
	{
		std::vector<TRayNode*> tempNodes;

		// FIX ME : Check for the performace issue not sure efficent or not
		for (auto i = 0; i < mTreziNodesList.size(); i++)
		{
			if (mTreziNodesList[i]->GetMeshIndices().size() != 0)
			{
				tempNodes.emplace_back(mTreziNodesList[i]);
			}
		}

		mTreziNodesList.clear();
		mTreziNodesList = std::move(tempNodes);
	}

	void Application::SaveScreenShot(std::string const& fileName)
	{
		if (mTracerPtr)
		{
			mTracerPtr->screenshot(fileName);
		}
	}

	void Application::Update(float DeltaTime)
	{
		/*if (mPauseRendering)
			return;*/

		if (mRayTracingService)
		{
			mRayTracingService->Update();
		}
	}

	bool Application::IsAppRunning() const
	{
		return mIsRunning && (mRayTracingService && mRayTracingService->GetTNApplication()->IsConnected());
	}

	const char* Application::GetTextureBuffer(const TRayMaterial* Mat, SurfaceTextureType Surface, int& OutBufferSize, int& OutWidth, int& OutHeight) const
	{
		int TextureInd = 0;
		OutBufferSize = 0;
		switch (Surface)
		{
		case SurfaceTextureType::DIFFUSE:
		{
			if (!Mat->isAlbedoTexFile)	TextureInd = Mat->albedoTexID;
			break;
		}
		case SurfaceTextureType::METALLIC:
		{
			if (!Mat->isMetallicTexFile)	TextureInd = Mat->metallicTexID;
			break;
		}
		case SurfaceTextureType::ROUGHNESS:
		{
			if (!Mat->isRoughnessTexFile)	TextureInd = Mat->roughnessTexID;
			break;
		}
		case SurfaceTextureType::SPECULAR:
		{
			if (!Mat->isSpecularTexFile)	TextureInd = Mat->specularTexID;
			break;
		}
		case SurfaceTextureType::EMISSIVE:
		{
			if (!Mat->isEmissiveTexFile)	TextureInd = Mat->emissiveTexID;
			break;
		}
		case SurfaceTextureType::BUMP:
		{
			if (!Mat->isBumpTexFile)	TextureInd = Mat->bumpTexID;
			break;
		}
		case SurfaceTextureType::NORMAL:
		{
			if (!Mat->isNormalTexFile)	TextureInd = Mat->normalTexID;
			break;
		}

		default:
			break;
		}

		if (TextureInd > 0)
		{
			std::map<int, TextureBufferData>::const_iterator itr = mTextureBuffers.find(TextureInd);
			if (itr != mTextureBuffers.end())
			{
				OutBufferSize = itr->second.Data->GetBufferSize();
				OutWidth = itr->second.Width;
				OutHeight = itr->second.Height;
				return (const char*)itr->second.Data->GetBuffer();
			}
		}

		return nullptr;
	}
}