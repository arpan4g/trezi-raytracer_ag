#include "inc/RayTracingService/SnapshotRequest.h"
#include <inc/math_utils/Matrix4.h>
#include <inc/TRayNode.h>
#include <inc/Application.h>
#include "inc/Json/json.hpp"
//#include <Utils/Constants.h>
#include "inc/Camera/Camera.h"
//#include "Utils\Macros.h"

namespace TRAY
{
	SnapshotRequest::~SnapshotRequest()
	{
		const int MeshCount = (int)mTreziMeshList.size();
		for (int i = 0; i < MeshCount; i++)
		{
			delete mTreziMeshList[i];
		}

		const int NodeCount = (int)mTreziNodesList.size();
		for (int i = 0; i < NodeCount; i++)
		{
			delete mTreziNodesList[i];
		}

		const int MatCount = (int)mMaterials.size();
		for (int i = 0; i < MatCount; i++)
		{
			delete mMaterials[i];
		}

		mTreziMeshList.clear();
		mTreziNodesList.clear();
		mMaterials.clear();
	}

	bool SnapshotRequest::CreateImageAndSendResponse(Application* TrayApp)
	{
		if (((TNRequest*)mSnapshotRequest.Get())->GetRequestType() == TNRequestType::RT_Snapshot)
		{
			int ImageWidth, ImageHeight;
			TNDataBufferPtr Data = TrayApp->GetTracer()->GetRenderedBuffer(ImageWidth, ImageHeight);
			//TODO: After taking the render get the image data
			TNSnapshotRequest* Req = static_cast<TNSnapshotRequest*>(mSnapshotRequest.Get());
			Req->SendResponse<const char*, unsigned int>((const char*)Data->GetBuffer(), Data->GetBufferSize(), ImageWidth, ImageHeight);

			//TrayApp->SaveScreenShot();
			std::cout << "Rendering Screenshot completed... \n";

			return true;
		}
		else if (((TNRequest*)mSnapshotRequest.Get())->GetRequestType() == TNRequestType::RT_MultiSnapshot)
		{
			TNMultiSnapshotRequest* Req = (TNMultiSnapshotRequest*)(mSnapshotRequest.Get());

			int ImageWidth, ImageHeight;
			TNDataBufferPtr Data = TrayApp->GetTracer()->GetRenderedBuffer(ImageWidth, ImageHeight);
			
			mAllMultiRenderTextureData.push_back({ Data, ImageWidth, ImageHeight });

			//std::cout << "Rendering Screenshot " << mCurrentMultiRenderIndex << "completed... \n";

			mCurrentMultiRenderIndex++;

			const int SnapshotCount = Req->SnapshotCount();
			if (mCurrentMultiRenderIndex >= SnapshotCount)
			{
				std::vector<TNMultiSnapshotResponse::TextureData> AllTextureDatas;
				for (int i = 0; i < SnapshotCount; i++)
				{
					AllTextureDatas.push_back({ (const char*)mAllMultiRenderTextureData[i].TextureData->GetBuffer(), mAllMultiRenderTextureData[i].TextureData->GetBufferSize(), (uint32_t)mAllMultiRenderTextureData[i].Width, (uint32_t)mAllMultiRenderTextureData[i].Height });
				}

				Req->SendResponse<const std::vector<TNMultiSnapshotResponse::TextureData>&>(AllTextureDatas);

				return true;
			}
			else
			{
				const TNMultiSnapshotRequest::CameraTransform& CamTransform = *Req->GetCameraTransform(mCurrentMultiRenderIndex);
				const TNMatrix4x4& Mat = CamTransform.Transform;

				Matrix4F Matrix;
				memcpy_s(Matrix.elements, sizeof(Matrix.elements), Mat.m_Components, sizeof(TNMatrix4x4::m_Components));
				TrayApp->GetCamera()->SetCameraData(Matrix, Req->GetFOV(), Req->GetWidth(), Req->GetHeight(), PINHOLE);
			}
		}

		return false;
	}

	void SnapshotRequest::LoadScene(Application* App)
	{
		if (mSnapshotRequest != nullptr)
		{
			mCurrentMultiRenderIndex = 0;

			App->CleanUpScene();
				//TODO: Start rendering here...
			App->SetTRayMeshList(mTreziMeshList);
			App->SetTRayNodeList(mTreziNodesList);
			App->SetTRayMaterialList(mMaterials);

			std::map<int, Application::TextureBufferData> TexturesMap;
			const int TexCount = (int)mTextures.size();
			for (int i = 0; i < TexCount; i++)
			{
				TexturesMap.insert(std::pair<int, Application::TextureBufferData>(i, mTextures[i]));
			}

			App->SetTRayTextureBuffersList(TexturesMap);

			//const Vector4F FirstNodePos = mTreziNodesList[0]->WorldTransformMat().get_row(3);
			//mTRayApp->CleanUpEmptyNodeList();

			App->InitTracer();

			if (((TNRequest*)mSnapshotRequest.Get())->GetRequestType() == TNRequestType::RT_Snapshot)
			{
				TNSnapshotRequest* Req = (TNSnapshotRequest*)(mSnapshotRequest.Get());
				const TNMatrix4x4& Mat = Req->GetTransform();

				Matrix4F Matrix;
				memcpy_s(Matrix.elements, sizeof(Matrix.elements), Mat.m_Components, sizeof(TNMatrix4x4::m_Components));
				const float aspectRatio = Req->GetWidth() / Req->GetHeight();
				float width = Req->GetWidth();
				float height = Req->GetHeight();
#if IN_DEBUG_MODE
					width = 540;
					height = 540;
#else
				if (Req->Is360())
				{
					width = 4096;
					height = 2048;
				}
#endif
				//App->GetCamera()->Resize(Req->GetWidth(), Req->GetHeight());
				App->ReshapeBufferOutput(width, height);
				App->GetCamera()->SetCameraData(Matrix, Req->GetFOV(), width, height, PINHOLE);
				App->SetSunAngles(mSunTheta, mSunPhi);
				//App->GetCamera()->SetCameraData(Matrix, Req->GetFOV(), 960, 540, PINHOLE);

			}
			else if (((TNRequest*)mSnapshotRequest.Get())->GetRequestType() == TNRequestType::RT_MultiSnapshot)
			{
				TNMultiSnapshotRequest* Req = (TNMultiSnapshotRequest*)(mSnapshotRequest.Get());

				const int SnapshotCount = Req->SnapshotCount();
				if (SnapshotCount > 0)
				{
					const TNMultiSnapshotRequest::CameraTransform& CamTransform = *Req->GetCameraTransform(0);
					const TNMatrix4x4& Mat = CamTransform.Transform;

					Matrix4F Matrix;
					memcpy_s(Matrix.elements, sizeof(Matrix.elements), Mat.m_Components, sizeof(TNMatrix4x4::m_Components));
					const float aspectRatio = Req->GetWidth() / Req->GetHeight();
					//App->GetCamera()->Resize(Req->GetWidth(), Req->GetHeight());
					float width = Req->GetWidth();
					float height = Req->GetHeight();
					if (Req->Is360())
					{
#if IN_DEBUG_MODE
						width = 540;
						height = 540;
#else
						width = 4096;
						height = 2048;
#endif
					}
					App->ReshapeBufferOutput(width, height);
					//App->GetWindow()->ResizeWindow(width, height);
					
					App->GetCamera()->SetCameraData(Matrix, Req->GetFOV(), width, height, Req->Is360() ? SPHERICAL : PINHOLE);
					//App->GetCamera()->SetCameraData(Matrix, Req->GetFOV(), Req->GetWidth(), Req->GetHeight(), Req->Is360() ? SPHERICAL : PINHOLE);
				}
			}
			
			mTreziMeshsInds.clear();
			mTreziMeshList.clear();
			mTreziNodesList.clear();
			mTextures.clear();
			mTextureIndices.clear();
			mMaterials.clear();
			mMaterialIndices.clear();

			mCurrentMultiRenderIndex = 0;

			// *****************************************************************
			// TODO: Suraj set the light data from mLightData;
			// *****************************************************************

			mProcessing = true;
		}
	}

	void SnapshotRequest::OnMessageReceived(TNMessagePtr Message)
	{
		if (Message->GetMessageType() == TNMessageType::MT_Light)
		{
			TNMessageLightData* Light = static_cast<TNMessageLightData*>(Message.Get());
			const Vector3F Loc = Vector3F(Light->GetLocation().m_X, Light->GetLocation().m_Y, Light->GetLocation().m_Z);
			mLightData.lightLocation = Loc;

			const Vector3F Dir = Vector3F(Light->GetDirection().m_X, Light->GetDirection().m_Y, Light->GetDirection().m_Z);
			mLightData.lightDirection = Dir;

			auto normalizedDir = -Dir.norm();
			mSunTheta = acosf(normalizedDir.z);
			mSunPhi = atan2f(normalizedDir.y, normalizedDir.x);

			mLightData.lightColor = Vector3F(Light->GetColor().m_X, Light->GetColor().m_Y, Light->GetColor().m_Z);
			mLightData.lightIntensity = Light->GetIntensity();
		}
		else if (Message->GetMessageType() == TNMessageType::MT_MeshData)
		{
			TNMessageMeshData* MData = static_cast<TNMessageMeshData*>(Message.Get());
			const int Ind = (int)mTreziMeshList.size();


			TRayMesh* Mesh = ConstructTrayMesh(MData->GetMeshId(), MData->GetSectionIndex(), MData->GetVertices(), MData->GetUvs()
				, MData->GetNormals(), MData->GetTangents(), MData->GetIndices(), 0);

			TSecIndMeshInd SectionMIndex(MData->GetSectionIndex(), Ind);

			TMeshIndices::iterator found = mTreziMeshsInds.find(MData->GetMeshId().CString());
			if (found == mTreziMeshsInds.end())
			{
				mTreziMeshsInds.insert(TMeshIdMeshInds(MData->GetMeshId().CString(), TSecIndMeshIndArr()));
				found = mTreziMeshsInds.find(MData->GetMeshId().CString());
			}

			found->second.push_back(SectionMIndex);
			mTreziMeshList.push_back(Mesh);
		}
		else if (Message->GetMessageType() == TNMessageType::MT_Node)
		{
			TNMessageNode* TNode = static_cast<TNMessageNode*>(Message.Get());

			TMeshIndices::iterator found = mTreziMeshsInds.find(TNode->GetMeshId().CString());

			if (std::string(TNode->GetMeshId().CString()) != "" && found != mTreziMeshsInds.end())
			{
				Matrix4F Matrix;
				memcpy_s(Matrix.elements, sizeof(Matrix.elements), TNode->GetTransform().m_Components, sizeof(TNMatrix4x4::m_Components));
				Matrix = Matrix.transpose();

				std::vector<int> MeshInds;
				GetMeshIndices(found->second, MeshInds);

				const std::vector<TNString>& Materials = TNode->GetMaterials();
				const int MatCount = (int)Materials.size();
				std::map<int, int> NodeMaterialIndices;
				for (int i = 0; i < MatCount; i++)
				{
					if (Materials[i] != TNString::EmptyString)
					{
						TMaterialIndicesMap::const_iterator Itr = mMaterialIndices.find(Materials[i].CString());
						if (Itr != mTextureIndices.end())
						{
							//NodeMaterialIndices.insert(std::pair<int, int>(i, Itr->second));
							NodeMaterialIndices.insert(std::pair<int, int>(MeshInds[i], Itr->second));
						}
					}
				}

				TRayNode* Node = new TRayNode(TNode->GetNodeId().CString(), true, MeshInds, NodeMaterialIndices, Matrix.transpose());
				mTreziNodesList.push_back(Node);
			}
		}
		else if (Message->GetMessageType() == TNMessageType::MT_TextureData)
		{
			if (mTextures.size() == 0)
			{
				// 0 is default texture(No Texture)
				mTextures.push_back({ 0, 0, TNDataBufferPtr() });
			}

			TNMessageTextureData* TNode = static_cast<TNMessageTextureData*>(Message.Get());
			if (mTextureIndices.find(TNode->GetTextureId().CString()) == mTextureIndices.end())
			{
				mTextureIndices.insert(std::pair<std::string, int>(TNode->GetTextureId().CString(), (int)mTextures.size()));
				mTextures.push_back({ (int)TNode->GetWidth(0), (int)TNode->GetHeight(0), TNode->GetTextureData(0) });
			}
		}
		else if (Message->GetMessageType() == TNMessageType::MT_MaterialData)
		{
			if (mMaterials.size() == 0)
			{
				// 0 is default material
				mMaterials.push_back(new TRayMaterial());
			}

			TNMessageMaterialData* TNode = static_cast<TNMessageMaterialData*>(Message.Get());
			if (mMaterialIndices.find(TNode->GetMaterialId().CString()) == mMaterialIndices.end())
			{
				mMaterialIndices.insert(std::pair<std::string, int>(TNode->GetMaterialId().CString(), (int)mMaterials.size()));
				TRayMaterial* NewMat = new TRayMaterial();

				NewMat->albedoTexID = GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Albedo));
				NewMat->albedoColor = { TNode->GetAlbedo().m_X, TNode->GetAlbedo().m_Y, TNode->GetAlbedo().m_Z, TNode->GetAlbedo().m_W };

				NewMat->metallicTexID = GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Metallic));
				NewMat->metallic = TNode->GetMetallic();

				NewMat->roughnessTexID = GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Roughness));
				NewMat->roughness = TNode->GetRoughness();

				NewMat->specularTexID = GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Specular));
				NewMat->specular = TNode->GetSpecular();

				NewMat->emissiveTexID = GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Emissive));
				NewMat->emission = { TNode->GetEmissive().m_X, TNode->GetEmissive().m_Y, TNode->GetEmissive().m_Z, 1.0f };

				NewMat->bumpTexID = TNode->IsBump() ? GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Normal)) : 0;
				NewMat->normalTexID = !TNode->IsBump() ? GetIndexOfTexture(mTextureIndices, TNode->GetTextureId(TNMessageMaterialData::TT_Normal)) : 0;;

				NewMat->specularTint = 0.0f;
				NewMat->isTranslucent = TNode->IsTransparent();
				NewMat->tintDiffuseColor = Vector4F(TNode->GetTintColor().m_X, TNode->GetTintColor().m_Y, TNode->GetTintColor().m_Z, 1.0f);
				NewMat->isTintDiffuseEnabled = TNode->IsTintEnabled();

				NewMat->isAlbedoTexFile = false;
				NewMat->isMetallicTexFile = false;
				NewMat->isRoughnessTexFile = false;
				NewMat->isSpecularTexFile = false;
				NewMat->isEmissiveTexFile = false;
				NewMat->isBumpTexFile = false;
				NewMat->isNormalTexFile = false;

				mMaterials.push_back(NewMat);
			}
		}
		else
		{
			printf("\nSome message received\n");
		}
	}

	int SnapshotRequest::GetIndexOfTexture(const TTextureIndicesMap& TexIndices, const TNString& TextureId) const
	{
		TTextureIndicesMap::const_iterator Itr = TexIndices.find(TextureId.CString());
		if (Itr != TexIndices.end())
		{
			return Itr->second;
		}

		return 0;
	}

	void SnapshotRequest::OnRequestReceived(TNMessagePtr Request)
	{
		TNRequest* RequestObj = static_cast<TNRequest*>(Request.Get());
		if (Request->GetMessageType() == TNMessageType::MT_Request && RequestObj)
		{
			if (RequestObj->GetRequestType() == TNRequestType::RT_Snapshot)
			{
				mSnapshotRequest = Request;
			}
			else if (RequestObj->GetRequestType() == TNRequestType::RT_MultiSnapshot)
			{
				mSnapshotRequest = Request;
			}
		}
	}

	TRayMesh* SnapshotRequest::ConstructTrayMesh(const TNString& BaseName, unsigned int SectionInd, const TNVector3Array& Verts
		, const TNVector2Array& Uvs, const TNVector3Array& Normals, const TNVector3Array& Tangents, const TNUIntArray& Inds, int MatInd)
	{
		std::vector<Vector3F> TVerts;
		GetBuffer<Vector3F>(TVerts, Verts.GetData(), Verts.Count());

		std::vector<Vector2F> TUVs;
		GetBuffer<Vector2F>(TUVs, Uvs.GetData(), Uvs.Count());

		std::vector<Vector3F> TNormals;
		GetBuffer<Vector3F>(TNormals, Normals.GetData(), Normals.Count());


		std::vector<Vector3F> TTangents;
		GetBuffer<Vector3F>(TTangents, Tangents.GetData(), Tangents.Count());

		std::vector<unsigned int> TInds;
		GetBuffer<unsigned int>(TInds, Inds.GetData(), Inds.Count());

		std::string MeshName = std::string(BaseName.CString()) + std::string("_") + std::to_string(SectionInd);
		TRayMesh* Mesh = new TRayMesh(MeshName, TVerts, TNormals, TTangents, TUVs, TInds, MatInd);

		return Mesh;
	}

	void SnapshotRequest::GetMeshIndices(const TSecIndMeshIndArr& SectionMeshInds, std::vector<int>& OutMeshInds)
	{
		const int Count = (int)SectionMeshInds.size();
		for (int i = 0; i < Count; i++)
		{
			OutMeshInds.push_back(SectionMeshInds[i].second);
		}
	}

	bool SnapshotRequest::IsRequestCompleted() const
	{
		return mSnapshotRequest != nullptr;
	}

	void SnapshotRequest::SendRequestFeedback(float Percent)
	{
		if (mSnapshotRequest != nullptr)
		{
			TNSnapshotRequest* Req = (TNSnapshotRequest*)mSnapshotRequest.Get();
			Req->SendFeedback<float>(Percent);
		}
	}
}
