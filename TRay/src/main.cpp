#include "shaders/app_config.h"
#include "inc/Application.h"

#include <sutil.h>
#include <IL/il.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <inc/StaticFunc.h>

using namespace TRAY;

static Application* g_app = nullptr;
static bool displayGUI = false;

void printUsage(const std::string& argv0)
{
	std::cerr << "\nUsage: " << argv0 << " [options]\n";
	std::cerr <<
		"App Options:\n"
		"   ?	 | help | --help			Print this usage message and exit.\n"
		"  -w	 | --width <int>			Window client width  (512).\n"
		"  -h	 | --height <int>			Window client height (512).\n"
		"  -f	 | --load_file <filename>	load the input file.\n"
		"  -envf | --env_file <filename>	Environment HDR file path.\n"
		"App Keystrokes:\n"
		"  SPACE  Toggles ImGui display.\n"
		"\n"
		<< std::endl;
}


int main(int argc, char *argv[])
{
	// https://stackoverflow.com/questions/5995433/removing-console-window-for-glut-freeglut-glfw
	//FreeConsole();

	int  windowWidth = 800;
	int  windowHeight = 800;
	//std::string filePath = "Trezi/housing+complex"; ///*"Trezi/Technical_school-current_m_TZI_3D";*/ "Trezi/Advanced_Material4Spheres_TZI_3D";
	std::string filePath = "";// "Trezi/3_tiny_apartments";// "Trezi/Technical_school-current_m_TZI_3D";/* "Trezi/Advanced_Material4Spheres_TZI_3D";*/
	std::string environment = "kiara_9_dusk_2k.hdr";
	std::string filenameScreenshot = "TRay_Output.png";
	bool showViewer = false;


	// Parse the command line parameters.
	for (int i = 1; i < argc; ++i)
	{
		const std::string arg(argv[i]);

		if (arg == "--help" || arg == "help" || arg == "?")
		{
			printUsage(argv[0]);
			return 0;
		}
		else if (arg == "-w" || arg == "--width")
		{
			if (i == argc - 1)
			{
				std::cerr << "Option '" << arg << "' requires additional argument.\n";
				printUsage(argv[0]);
				return 0;
			}
			windowWidth = atoi(argv[++i]);
		}
		else if (arg == "-h" || arg == "--height") {
			if (i == argc - 1)
			{
				std::cerr << "Option '" << arg << "' requires additional argument.\n";
				printUsage(argv[0]);
				return 0;
			}
			windowHeight = atoi(argv[++i]);
		}
		else if (arg == "-f" || arg == "--load_file") {
			if (i == argc - 1)
			{
				std::cerr << "Option '" << arg << "' requires additional argument.\n";
				printUsage(argv[0]);
				return 0;
			}
			filePath = argv[++i];
		}
		else if (arg == "-envf" || arg == "--env_file") {
			if (i == argc - 1)
			{
				std::cerr << "Option '" << arg << "' requires additional argument.\n";
				printUsage(argv[0]);
				return 0;
			}
			environment = argv[++i];
		}
		else if (arg == "-s" || arg == "--save") {
			if (i == argc - 1)
			{
				std::cerr << "Option '" << arg << "' requires additional argument.\n";
				printUsage(argv[0]);
				return 0;
			}
			filenameScreenshot = argv[++i];
			showViewer = false; // Do not render the GUI when just taking a screenshot. (Automated QA feature.)
		}
		else {
			std::cerr << "Unknown option '" << arg << "'\n";
			printUsage(argv[0]);
			return 0;
		}
	}


	int  devices = 3210;  // Decimal digits encode OptiX device ordinals. Default 3210 means to use all four first installed devices, when available.
	bool interop = true;  // Use OpenGL interop Pixel-Bufferobject to display the resulting image. Disable this when running on multi-GPU or TCC driver mode.
	int  stackSize = 2048;  // Command line parameter just to be able to find the smallest working size.
	bool light = false; // Add a geometric are light. Best used with miss 0 and 1.
	int  miss = 2;     // Select the environment light (0 = black, no light; 1 = constant white environment; 3 = spherical environment texture.

	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		error_callback(1, "GLFW failed to initialize.");
		return 1;
	}

	glfwWindowHint(GLFW_VISIBLE, showViewer);
	GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "TRAY Demo", NULL, NULL);
	if (!window)
	{
		error_callback(2, "glfwCreateWindow() failed.");
		glfwTerminate();
		return 2;
	}

	glfwMakeContextCurrent(window);

	if (glewInit() != GL_NO_ERROR)
	{
		error_callback(3, "GLEW failed to initialize.");
		glfwTerminate();
		return 3;
	}

	ilInit(); // Initialize DevIL once.

	g_app = new Application(window, filePath.c_str(), windowWidth, windowHeight, environment);

	glfwPollEvents(); // Render continuously.

	glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
	g_app->ReshapeBufferOutput(windowWidth, windowHeight);


	// Main loop
	while (!glfwWindowShouldClose(window) && g_app->IsAppRunning())
	{
		if (showViewer)
		{
			glfwPollEvents(); // Render continuously.

			glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
			g_app->ReshapeBufferOutput(windowWidth, windowHeight);

			g_app->Update(0.01f);
			g_app->RenderGUIFrame();

			g_app->Render();  // OptiX rendering and OpenGL texture update.
			g_app->Display();

			glfwSwapBuffers(window);
		}
		else
		{
			g_app->Update(0.01f);
			g_app->Render();  // OptiX rendering and OpenGL texture update.
		}

		//glfwWaitEvents(); // Render only when an event is happening. Needs some glfwPostEmptyEvent() to prevent GUI lagging one frame behind when ending an action.
	}

	g_app->SaveScreenShot(filenameScreenshot);

	// Cleanup
	delete g_app;

	ilShutDown();

	glfwTerminate();

	return 0;
}



