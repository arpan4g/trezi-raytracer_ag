@echo off
SET OptiX_Path=%1
REM SET Varch_Home=%2

if not defined OptiX_Path (SET OptiX_Path="C:\\ProgramData\\NVIDIA Corporation\\OptiX SDK 6.0.0")
REM if not defined Varch_Home (SET VARCH_HOME="C:\\ProgramData\\NVIDIA Corporation\\OptiX SDK 5.1.1")

@echo OPTIX_PATH : %OptiX_Path%

if not exist buildsWin64 (mkdir buildsWin64)
cd buildsWin64

cmake -G "Visual Studio 15 2017 Win64" -DOptiX_INSTALL_DIR=%Optix_Path%  -DVarch_Home=%VARCH_HOME% ..
cd ..
